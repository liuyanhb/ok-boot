/***********************************************************************************
 * 文件名： HC32L13X_FunDef.h
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		自定义的外设操作库，官方提供的库函数操作效率低、占用高，这里定义的都是直接操作寄存器
 *  的宏。这种方式可以同样用于STM32等Cortex-M系列内核MCU。
 *      hc32xxxx.h 中已经定义好了寄存器，且使用位偏移结构体定义好了位访问。
 *      HC32L13X 系列特有的操作宏。
 *      本文件作为 HC32FunctionDefine.h 的一部分，不需要特意包含本文件，包含 HC32FunctionDefine.h 即可。
 * 修改记录：
 *  2020/8/19: 初版。 刘言。
***********************************************************************************/

typedef enum en_gpio_port
{
    GPIO_PORT_A = 0,                 ///< GPIO PORT A
    GPIO_PORT_B = 1,                 ///< GPIO PORT B
    GPIO_PORT_C = 2,                 ///< GPIO PORT C
    GPIO_PORT_D = 3,                 ///< GPIO PORT D
}en_gpio_port_t;

#define GPIO_PA01_SEL_GPIO_PA01         M0P_GPIO->PA01_SEL = 0
#define GPIO_PA01_SEL_UART1_RTS         M0P_GPIO->PA01_SEL = 1
#define GPIO_PA01_SEL_LPUART1_RXD       M0P_GPIO->PA01_SEL = 2
/*待补充*/

#define GPIO_PA02_SEL_GPIO_PA02         M0P_GPIO->PA02_SEL = 0
/*待补充*/

#define GPIO_PA03_SEL_GPIO_PA03         M0P_GPIO->PA03_SEL = 0
/*待补充*/

#define GPIO_PA09_SEL_GPIO_PA09         M0P_GPIO->PA09_SEL = 0
#define GPIO_PA09_SEL_UART0_TXD         M0P_GPIO->PA09_SEL = 1

#define GPIO_PA10_SEL_GPIO_PA10         M0P_GPIO->PA10_SEL = 0
#define GPIO_PA10_SEL_UART0_RXD         M0P_GPIO->PA10_SEL = 1

#define GPIO_PD00_SEL_GPIO_PD00         M0P_GPIO->PD00_SEL = 0
#define GPIO_PD00_SEL_I2C0_SDA          M0P_GPIO->PD00_SEL = 1
#define GPIO_PD00_SEL_UART1_TXD         M0P_GPIO->PD00_SEL = 3

#define GPIO_PD01_SEL_GPIO_PD01         M0P_GPIO->PD01_SEL = 0
#define GPIO_PD01_SEL_I2C0_SCL          M0P_GPIO->PD01_SEL = 1
#define GPIO_PD01_SEL_TIM4_CHB          M0P_GPIO->PD01_SEL = 2
#define GPIO_PD01_SEL_UART1_RXD         M0P_GPIO->PD01_SEL = 3

#define GPIO_PD05_SEL_GPIO_PD05         M0P_GPIO->PD05_SEL = 0


#define GPIO_PD06_SEL_GPIO_PD06         M0P_GPIO->PD06_SEL = 0












