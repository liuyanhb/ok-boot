#ifndef _HC32_LYLIB_H_
#define _HC32_LYLIB_H_

#ifdef __cplusplus
 extern "C" {
#endif


// MCU型号系列定义

#ifndef HC32F005
    #define HC32F005    0
#endif
#ifndef HC32L13X
    #define HC32L13X    1
#endif

#include "typedef.h"

#include "HC32_LyLibConfig.h"


// 依据型号包含头文件

#ifndef HC32_MCU_SERIES
    #error "未定义 HC32_MCU_SERIES"
#else
    #if (HC32_MCU_SERIES == HC32F005)
        #include "hc32f005.h"
    #elif (HC32_MCU_SERIES == HC32L13X)
        #include "hc32l13x.h"
    #endif

#endif



#include "HC32_FunctionDefine.h"

#ifdef __cplusplus
}
#endif

#endif


