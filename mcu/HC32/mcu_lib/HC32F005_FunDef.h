/***********************************************************************************
 * 文件名： HC32F005_FunDef.h
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		自定义的外设操作库，官方提供的库函数操作效率低、占用高，这里定义的都是直接操作寄存器
 *  的宏。这种方式可以同样用于STM32等Cortex-M系列内核MCU。
 *      hc32xxxx.h 中已经定义好了寄存器，且使用位偏移结构体定义好了位访问。
 *      HC32F005 系列特有的操作宏。
 *      本文件作为 HC32FunctionDefine.h 的一部分，不需要特意包含本文件，包含 HC32FunctionDefine.h 即可。
 * 修改记录：
 *  2020/8/19: 初版。 刘言。
***********************************************************************************/

typedef enum en_gpio_port
{
    GPIO_PORT_0 = 0,                 ///< GPIO PORT 0
    GPIO_PORT_1 = 1,                 ///< GPIO PORT 1
    GPIO_PORT_2 = 2,                 ///< GPIO PORT 2
    GPIO_PORT_3 = 3,                 ///< GPIO PORT 3
}en_gpio_port_t;

#define GPIO_P01_SEL_GPIO_P01           M0P_GPIO->P01_SEL = 0
#define GPIO_P01_SEL_UART0_RXD          M0P_GPIO->P01_SEL = 1
#define GPIO_P01_SEL_I2C_SDA            M0P_GPIO->P01_SEL = 2
#define GPIO_P01_SEL_UART1_TXD          M0P_GPIO->P01_SEL = 3
#define GPIO_P01_SEL_TIM0_TOG           M0P_GPIO->P01_SEL = 4
#define GPIO_P01_SEL_TIM5_CHB           M0P_GPIO->P01_SEL = 5
#define GPIO_P01_SEL_SPI_SCK            M0P_GPIO->P01_SEL = 6
#define GPIO_P01_SEL_TIM2_EXT           M0P_GPIO->P01_SEL = 7

#define GPIO_P02_SEL_GPIO_P02           M0P_GPIO->P02_SEL = 0
#define GPIO_P02_SEL_UART0_TXD          M0P_GPIO->P02_SEL = 1
#define GPIO_P02_SEL_I2C_SCL            M0P_GPIO->P02_SEL = 2
#define GPIO_P02_SEL_UART1_RXD          M0P_GPIO->P02_SEL = 3
#define GPIO_P02_SEL_TIM0_TOGN          M0P_GPIO->P02_SEL = 4
#define GPIO_P02_SEL_TIM6_CHA           M0P_GPIO->P02_SEL = 5
#define GPIO_P02_SEL_SPI_CS             M0P_GPIO->P02_SEL = 6
#define GPIO_P02_SEL_TIM2_GATE          M0P_GPIO->P02_SEL = 7
/*待补充*/

#define GPIO_P03_SEL_GPIO_P03           M0P_GPIO->P03_SEL = 0
#define GPIO_P03_SEL_PCA_CH3            M0P_GPIO->P03_SEL = 1
#define GPIO_P03_SEL_SPI_CS             M0P_GPIO->P03_SEL = 2
#define GPIO_P03_SEL_TIM6_CHB           M0P_GPIO->P03_SEL = 3
#define GPIO_P03_SEL_LPTIM_EXT          M0P_GPIO->P03_SEL = 4
#define GPIO_P03_SEL_RTC_1HZ            M0P_GPIO->P03_SEL = 5
#define GPIO_P03_SEL_PCA_ECI            M0P_GPIO->P03_SEL = 6
#define GPIO_P03_SEL_VC0_OUT            M0P_GPIO->P03_SEL = 7


#define GPIO_P15_SEL_GPIO_P15           M0P_GPIO->P15_SEL = 0
#define GPIO_P15_SEL_I2C_SDA            M0P_GPIO->P15_SEL = 1
#define GPIO_P15_SEL_TIM2_TOG           M0P_GPIO->P15_SEL = 2
#define GPIO_P15_SEL_TIM4_CHB           M0P_GPIO->P15_SEL = 3
#define GPIO_P15_SEL_LPTIM_GATE         M0P_GPIO->P15_SEL = 4
#define GPIO_P15_SEL_SPI_SCK            M0P_GPIO->P15_SEL = 5
#define GPIO_P15_SEL_UART0_RXD          M0P_GPIO->P15_SEL = 6
#define GPIO_P15_SEL_LVD_OUT            M0P_GPIO->P15_SEL = 7


#define GPIO_P23_SEL_GPIO_P23           M0P_GPIO->P23_SEL = 0
#define GPIO_P23_SEL_TIM6_CHA           M0P_GPIO->P23_SEL = 1
#define GPIO_P23_SEL_TIM4_CHB           M0P_GPIO->P23_SEL = 2
#define GPIO_P23_SEL_TIM4_CHA           M0P_GPIO->P23_SEL = 3
#define GPIO_P23_SEL_PCA_CH0            M0P_GPIO->P23_SEL = 4
#define GPIO_P23_SEL_SPI_MOSI           M0P_GPIO->P23_SEL = 5
#define GPIO_P23_SEL_UART1_TXD          M0P_GPIO->P23_SEL = 6
#define GPIO_P23_SEL_IR_OUT             M0P_GPIO->P23_SEL = 7

#define GPIO_P25_SEL_GPIO_P25           M0P_GPIO->P25_SEL = 0
#define GPIO_P25_SEL_SPI_SCK            M0P_GPIO->P25_SEL = 1
#define GPIO_P25_SEL_PCA_CH0            M0P_GPIO->P25_SEL = 2
#define GPIO_P25_SEL_TIM5_CHA           M0P_GPIO->P25_SEL = 3
#define GPIO_P25_SEL_LVD_OUT            M0P_GPIO->P25_SEL = 4
#define GPIO_P25_SEL_LPUART_RXD         M0P_GPIO->P25_SEL = 5
#define GPIO_P25_SEL_I2C_SDA            M0P_GPIO->P25_SEL = 6
#define GPIO_P25_SEL_TIM1_GATE          M0P_GPIO->P25_SEL = 7

#define GPIO_P27_SEL_GPIO_P27           M0P_GPIO->P27_SEL = 0
#define GPIO_P27_SEL_SPI_MOSI           M0P_GPIO->P27_SEL = 1
#define GPIO_P27_SEL_TIM5_CHA           M0P_GPIO->P27_SEL = 2
#define GPIO_P27_SEL_TIM6_CHA           M0P_GPIO->P27_SEL = 3
#define GPIO_P27_SEL_PCA_CH3            M0P_GPIO->P27_SEL = 4
#define GPIO_P27_SEL_UART0_RXD          M0P_GPIO->P27_SEL = 5
#define GPIO_P27_SEL_RCH_OUT            M0P_GPIO->P27_SEL = 6
#define GPIO_P27_SEL_XTH_OUT            M0P_GPIO->P27_SEL = 7

#define GPIO_P31_SEL_GPIO_P31           M0P_GPIO->P31_SEL = 0
#define GPIO_P31_SEL_LPTIM_TOG          M0P_GPIO->P31_SEL = 1
#define GPIO_P31_SEL_PCA_ECI            M0P_GPIO->P31_SEL = 2
#define GPIO_P31_SEL_PCLK_OUT           M0P_GPIO->P31_SEL = 3
#define GPIO_P31_SEL_VC0_OUT            M0P_GPIO->P31_SEL = 4
#define GPIO_P31_SEL_UART0_TXD          M0P_GPIO->P31_SEL = 5
#define GPIO_P31_SEL_RCL_OUT            M0P_GPIO->P31_SEL = 6
#define GPIO_P31_SEL_HCLK_OUT           M0P_GPIO->P31_SEL = 7

#define GPIO_P32_SEL_GPIO_P32           M0P_GPIO->P32_SEL = 0
#define GPIO_P32_SEL_LPTIM_TOGN         M0P_GPIO->P32_SEL = 1
#define GPIO_P32_SEL_PCA_CH2            M0P_GPIO->P32_SEL = 2
#define GPIO_P32_SEL_TIM6_CHB           M0P_GPIO->P32_SEL = 3
#define GPIO_P32_SEL_VC1_OUT            M0P_GPIO->P32_SEL = 4
#define GPIO_P32_SEL_UART1_TXD          M0P_GPIO->P32_SEL = 5
#define GPIO_P32_SEL_PCA_CH4            M0P_GPIO->P32_SEL = 6
#define GPIO_P32_SEL_RTC_1HZ            M0P_GPIO->P32_SEL = 7

#define GPIO_P33_SEL_GPIO_P33           M0P_GPIO->P33_SEL = 0
#define GPIO_P33_SEL_LPUART_RXD         M0P_GPIO->P33_SEL = 1
#define GPIO_P33_SEL_PCA_CH1            M0P_GPIO->P33_SEL = 2
#define GPIO_P33_SEL_TIM5_CHB           M0P_GPIO->P33_SEL = 3
#define GPIO_P33_SEL_PCA_ECI            M0P_GPIO->P33_SEL = 4
#define GPIO_P33_SEL_UART1_RXD          M0P_GPIO->P33_SEL = 5
#define GPIO_P33_SEL_XTL_OUT            M0P_GPIO->P33_SEL = 6
#define GPIO_P33_SEL_TIM1_TOGN          M0P_GPIO->P33_SEL = 7

#define GPIO_P35_SEL_GPIO_P35           M0P_GPIO->P35_SEL = 0
#define GPIO_P35_SEL_UART1_TXD          M0P_GPIO->P35_SEL = 1
#define GPIO_P35_SEL_TIM6_CHB           M0P_GPIO->P35_SEL = 2
#define GPIO_P35_SEL_UART0_TXD          M0P_GPIO->P35_SEL = 3
#define GPIO_P35_SEL_TIM0_GATE          M0P_GPIO->P35_SEL = 4
#define GPIO_P35_SEL_TIM4_CHB           M0P_GPIO->P35_SEL = 5
#define GPIO_P35_SEL_SPI_MISO           M0P_GPIO->P35_SEL = 6
#define GPIO_P35_SEL_I2C_SDA            M0P_GPIO->P35_SEL = 7

#define GPIO_P36_SEL_GPIO_P36           M0P_GPIO->P36_SEL = 0
#define GPIO_P36_SEL_UART1_RXD          M0P_GPIO->P36_SEL = 1
#define GPIO_P36_SEL_TIM6_CHA           M0P_GPIO->P36_SEL = 2
#define GPIO_P36_SEL_UART0_RXD          M0P_GPIO->P36_SEL = 3
#define GPIO_P36_SEL_PCA_CH4            M0P_GPIO->P36_SEL = 4
#define GPIO_P36_SEL_TIM5_CHA           M0P_GPIO->P36_SEL = 5
#define GPIO_P36_SEL_SPI_MOSI           M0P_GPIO->P36_SEL = 6
#define GPIO_P36_SEL_I2C_SCL            M0P_GPIO->P36_SEL = 7











