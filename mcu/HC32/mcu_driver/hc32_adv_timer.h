/***********************************************************************************
 * 文件名： hc32_adv_timer.h
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		HC32系列的高级定时器驱动，功能较多，不断完善中。
 *      中断入口统一定义在 isr.c 中。
 * 修改记录：
 * 	2020/10/26: 初版。 刘言。
***********************************************************************************/

#ifndef _HC32_ADV_TIMER_H_
#define _HC32_ADV_TIMER_H_

#include "HC32_LyLib.h"

#ifdef __cplusplus
 extern "C" {
#endif 


#ifndef HC32_EXIST_CONFIG_FILE

// 初始参数定义，对应的Init函数采用这些参数，某些参数可以通过特定API在应用中更改。

// TIM4初始参数

#define TIM4_CMODE          0       // 计数模式：0-锯齿波A模式，4-三角波A模式，5-三角波B模式。不要设置其他值。
#define TIM4_CDIR           1       // 计数方向：0-递减，1-递加
#define TIM4_CKDIV          0       // 计数时钟分频：0~7分别代表1、2、4、8、16、64、256、1024分频
#define TIM4_CYCLE          120     // 计数周期值 1~65535
#define TIM4_DTCEN          0       // 1-死区功能有效，0-关闭
#define TIM4_SEPA           0       // 1-DTUA和DTDA的值自动相等，0-分别设置
#define TIM4_DTUA           0       // 向上计数时的死区时间 1~65535，GCMBR = GCMAR - DTUAR
#define TIM4_DTDA           0       // 向下计数时的死区时间 1~65535，GCMBR = GCMAR - DTDAR
#define TIM4_EN_INTA        0       // 1-使能通道A匹配或者捕获事件中断  
#define TIM4_EN_INTB        0       // 1-使能通道B匹配或者捕获事件中断  
#define TIM4_EN_INTOV       0       // 1-使能计数上溢中断  
#define TIM4_EN_INTUD       0       // 1-使能计数下溢中断  
#define TIM4_EN_INTDE       0       // 1-使能死区时间错误异常中断
#define TIM4_RUN            0       // 1-初始化后立即开始运行（计数），0-初始化后不运行

#define TIM4_CHA_VALUE      2       // 比较/捕获值 1~65535。（与计数值相等时输出翻转，三角波模式下PWM占空比=TIM4_CHA_VALUE/TIM4_CYCLE）
#define TIM4_CHA_EN_BUFF    1       // 1-启用缓存，仅在计数周期结束时更新新的值，0-不启用
#define TIM4_CHA_MODE       0       // 模式：0-比较输出模式，1-捕获输入模式
#define TIM4_CHA_EN_PORT    0       // 1-端口使能，0-失能
#define TIM4_CHA_EPA        2       // 计数值与周期值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM4_CHA_ECA        2       // 计数值与比较值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM4_CHA_SELM       0       // 计数开始或停止时（起始结束）电平模式：0-设置（指定）的电平，1-保持之前的电平不变化
#define TIM4_CHA_SL         0       // 起始（计数开始时）电平：0-低电平，1-高电平。TIM4_CHA_SELM = 1 时无效。
#define TIM4_CHA_EL         0       // 结束（计数停止时）电平：0-低电平，1-高电平。TIM4_CHA_SELM = 1 时无效。
#define TIM4_CHA_BKS        0       // 比较输出时刹车触发信号源：0-VC中断标志，1-AB通道电平相同，2-进入低功耗，3-端口或软件控制
#define TIM4_CHA_BKL        0       // 刹车电平状态：0-无动作（刹车无效），1-高阻，2-低电平，3-高电平
#define TIM4_CHA_EN_FILT    0       // 1-使能捕获输入滤波，0-不使用。（使能后连续3次检测到相同电平才有效）
#define TIM4_CHA_FILT_CK    0       // 滤波时钟分频：0-PCLK,1-PCLK/4,2-PCLK/16,3-PCLK/64

#define TIM4_CHB_VALUE      60      // 比较/捕获值 0~65535。（互补输出时启用死区设置后，该值无效）
#define TIM4_CHB_EN_BUFF    1       // 1-启用缓存，仅在计数周期结束时更新新的比较值，0-不启用 （互补输出时启用死区设置后，该值应保持0）
#define TIM4_CHB_MODE       0       // 模式：0-比较输出模式，1-捕获输入模式
#define TIM4_CHB_EN_PORT    1       // 1-端口使能，0-失能
#define TIM4_CHB_EPA        1       // 计数值与周期值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM4_CHB_ECA        0       // 计数值与比较值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM4_CHB_SELM       0       // 计数开始或停止时（起始结束）电平模式：0-设置（指定）的电平，1-保持之前的电平不变化
#define TIM4_CHB_SL         1       // 起始（计数开始时）电平：0-低电平，1-高电平。TIM4_CHA_SELM = 1 时无效。
#define TIM4_CHB_EL         0       // 结束（计数停止时）电平：0-低电平，1-高电平。TIM4_CHA_SELM = 1 时无效。
#define TIM4_CHB_BKS        0       // 比较输出时刹车触发信号源：0-VC中断标志，1-AB通道电平相同，2-进入低功耗，3-端口或软件控制
#define TIM4_CHB_BKL        2       // 刹车电平状态：0-无动作（刹车无效），1-高阻，2-低电平，3-高电平
#define TIM4_CHB_EN_FILT    0       // 1-使能捕获输入滤波，0-不使用。（使能后连续3次检测到相同电平才有效）
#define TIM4_CHB_FILT_CK    0       // 滤波采样时钟分频：0-PCLK,1-PCLK/4,2-PCLK/16,3-PCLK/64

// TIM5初始参数

#define TIM5_CMODE          4       // 计数模式：0-锯齿波A模式，4-三角波A模式，5-三角波B模式。不要设置其他值。
#define TIM5_CDIR           1       // 计数方向：0-递减，1-递加
#define TIM5_CKDIV          7       // 计数时钟分频：0~7分别代表1、2、4、8、16、64、256、1024分频
#define TIM5_CYCLE          10000   // 计数周期值 1~65535
#define TIM5_DTCEN          0       // 1-死区功能有效，0-关闭
#define TIM5_SEPA           0       // 1-DTUA和DTDA的值自动相等，0-分别设置
#define TIM5_DTUA           0       // 向上计数时的死区时间 1~65535，GCMBR = GCMAR - DTUAR
#define TIM5_DTDA           0       // 向下计数时的死区时间 1~65535，GCMBR = GCMAR - DTDAR
#define TIM5_EN_INTA        0       // 1-使能通道A匹配或者捕获事件中断  
#define TIM5_EN_INTB        0       // 1-使能通道B匹配或者捕获事件中断  
#define TIM5_EN_INTOV       0       // 1-使能计数上溢中断  
#define TIM5_EN_INTUD       0       // 1-使能计数下溢中断  
#define TIM5_EN_INTDE       0       // 1-使能死区时间错误异常中断
#define TIM5_RUN            0       // 1-初始化后立即开始运行（计数），0-初始化后不运行

#define TIM5_CHA_VALUE      5000    // 比较/捕获值 1~65535。（与计数值相等时输出翻转，三角波模式下PWM占空比=TIM5_CHA_VALUE/TIM5_CYCLE）
#define TIM5_CHA_EN_BUFF    1       // 1-启用缓存，仅在计数周期结束时更新新的值，0-不启用
#define TIM5_CHA_MODE       0       // 模式：0-比较输出模式，1-捕获输入模式
#define TIM5_CHA_EN_PORT    0       // 1-端口使能，0-失能
#define TIM5_CHA_EPA        2       // 计数值与周期值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM5_CHA_ECA        2       // 计数值与比较值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM5_CHA_SELM       0       // 计数开始或停止时（起始结束）电平模式：0-设置（指定）的电平，1-保持之前的电平不变化
#define TIM5_CHA_SL         0       // 起始（计数开始时）电平：0-低电平，1-高电平。TIM5_CHA_SELM = 1 时无效。
#define TIM5_CHA_EL         0       // 结束（计数停止时）电平：0-低电平，1-高电平。TIM5_CHA_SELM = 1 时无效。
#define TIM5_CHA_BKS        0       // 比较输出时刹车触发信号源：0-VC中断标志，1-AB通道电平相同，2-进入低功耗，3-端口或软件控制
#define TIM5_CHA_BKL        0       // 刹车电平状态：0-无动作（刹车无效），1-高阻，2-低电平，3-高电平
#define TIM5_CHA_EN_FILT    0       // 1-使能捕获输入滤波，0-不使用。（使能后连续3次检测到相同电平才有效）
#define TIM5_CHA_FILT_CK    0       // 滤波时钟分频：0-PCLK,1-PCLK/4,2-PCLK/16,3-PCLK/64

#define TIM5_CHB_VALUE      5000    // 比较/捕获值 1~65535。（互补输出时启用死区设置后，该值无效）
#define TIM5_CHB_EN_BUFF    1       // 1-启用缓存，仅在计数周期结束时更新新的比较值，0-不启用 （互补输出时启用死区设置后，该值应保持0）
#define TIM5_CHB_MODE       0       // 模式：0-比较输出模式，1-捕获输入模式
#define TIM5_CHB_EN_PORT    0       // 1-端口使能，0-失能
#define TIM5_CHB_EPA        2       // 计数值与周期值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM5_CHB_ECA        2       // 计数值与比较值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM5_CHB_SELM       0       // 计数开始或停止时（起始结束）电平模式：0-设置（指定）的电平，1-保持之前的电平不变化
#define TIM5_CHB_SL         0       // 起始（计数开始时）电平：0-低电平，1-高电平。TIM5_CHA_SELM = 1 时无效。
#define TIM5_CHB_EL         0       // 结束（计数停止时）电平：0-低电平，1-高电平。TIM5_CHA_SELM = 1 时无效。
#define TIM5_CHB_BKS        0       // 比较输出时刹车触发信号源：0-VC中断标志，1-AB通道电平相同，2-进入低功耗，3-端口或软件控制
#define TIM5_CHB_BKL        0       // 刹车电平状态：0-无动作（刹车无效），1-高阻，2-低电平，3-高电平
#define TIM5_CHB_EN_FILT    0       // 1-使能捕获输入滤波，0-不使用。（使能后连续3次检测到相同电平才有效）
#define TIM5_CHB_FILT_CK    0       // 滤波采样时钟分频：0-PCLK,1-PCLK/4,2-PCLK/16,3-PCLK/64

// TIM6初始参数

#define TIM6_CMODE          4       // 计数模式：0-锯齿波A模式，4-三角波A模式，5-三角波B模式。不要设置其他值。
#define TIM6_CDIR           1       // 计数方向：0-递减，1-递加
#define TIM6_CKDIV          0       // 计数时钟分频：0~7分别代表1、2、4、8、16、64、256、1024分频
#define TIM6_CYCLE          4       // 计数周期值 1~65535
#define TIM6_DTCEN          1       // 1-死区功能有效，0-关闭
#define TIM6_SEPA           0       // 1-DTUA和DTDA的值自动相等，0-分别设置
#define TIM6_DTUA           0       // 向上计数时的死区时间 1~65535，GCMBR = GCMAR - DTUAR
#define TIM6_DTDA           0       // 向下计数时的死区时间 1~65535，GCMBR = GCMAR - DTDAR
#define TIM6_EN_INTA        0       // 1-使能通道A匹配或者捕获事件中断  
#define TIM6_EN_INTB        0       // 1-使能通道B匹配或者捕获事件中断  
#define TIM6_EN_INTOV       0       // 1-使能计数上溢中断  
#define TIM6_EN_INTUD       0       // 1-使能计数下溢中断  
#define TIM6_EN_INTDE       0       // 1-使能死区时间错误异常中断
#define TIM6_RUN            0       // 1-初始化后立即开始运行（计数），0-初始化后不运行

#define TIM6_CHA_VALUE      2       // 比较/捕获值 1~65535。（与计数值相等时输出翻转，三角波模式下PWM占空比=TIM6_CHA_VALUE/TIM6_CYCLE）
#define TIM6_CHA_EN_BUFF    1       // 1-启用缓存，仅在计数周期结束时更新新的值，0-不启用
#define TIM6_CHA_MODE       0       // 模式：0-比较输出模式，1-捕获输入模式
#define TIM6_CHA_EN_PORT    1       // 1-端口使能，0-失能
#define TIM6_CHA_EPA        2       // 计数值与周期值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM6_CHA_ECA        3       // 计数值与比较值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM6_CHA_SELM       0       // 计数开始或停止时（起始结束）电平模式：0-设置（指定）的电平，1-保持之前的电平不变化
#define TIM6_CHA_SL         0       // 起始（计数开始时）电平：0-低电平，1-高电平。TIM6_CHA_SELM = 1 时无效。
#define TIM6_CHA_EL         0       // 结束（计数停止时）电平：0-低电平，1-高电平。TIM6_CHA_SELM = 1 时无效。
#define TIM6_CHA_BKS        0       // 比较输出时刹车触发信号源：0-VC中断标志，1-AB通道电平相同，2-进入低功耗，3-端口或软件控制
#define TIM6_CHA_BKL        0       // 刹车电平状态：0-无动作（刹车无效），1-高阻，2-低电平，3-高电平
#define TIM6_CHA_EN_FILT    0       // 1-使能捕获输入滤波，0-不使用。（使能后连续3次检测到相同电平才有效）
#define TIM6_CHA_FILT_CK    0       // 滤波时钟分频：0-PCLK,1-PCLK/4,2-PCLK/16,3-PCLK/64

#define TIM6_CHB_VALUE      2       // 比较/捕获值 1~65535。（互补输出时启用死区设置后，该值无效）
#define TIM6_CHB_EN_BUFF    0       // 1-启用缓存，仅在计数周期结束时更新新的比较值，0-不启用 （互补输出时启用死区设置后，该值应保持0）
#define TIM6_CHB_MODE       0       // 模式：0-比较输出模式，1-捕获输入模式
#define TIM6_CHB_EN_PORT    1       // 1-端口使能，0-失能
#define TIM6_CHB_EPA        2       // 计数值与周期值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM6_CHB_ECA        3       // 计数值与比较值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM6_CHB_SELM       0       // 计数开始或停止时（起始结束）电平模式：0-设置（指定）的电平，1-保持之前的电平不变化
#define TIM6_CHB_SL         1       // 起始（计数开始时）电平：0-低电平，1-高电平。TIM6_CHA_SELM = 1 时无效。
#define TIM6_CHB_EL         1       // 结束（计数停止时）电平：0-低电平，1-高电平。TIM6_CHA_SELM = 1 时无效。
#define TIM6_CHB_BKS        0       // 比较输出时刹车触发信号源：0-VC中断标志，1-AB通道电平相同，2-进入低功耗，3-端口或软件控制
#define TIM6_CHB_BKL        0       // 刹车电平状态：0-无动作（刹车无效），1-高阻，2-低电平，3-高电平
#define TIM6_CHB_EN_FILT    0       // 1-使能捕获输入滤波，0-不使用。（使能后连续3次检测到相同电平才有效）
#define TIM6_CHB_FILT_CK    0       // 滤波采样时钟分频：0-PCLK,1-PCLK/4,2-PCLK/16,3-PCLK/64

#endif



// APIs

void HC32TIM4_Init();

STATIC_FORCE_IN_LINE void HC32TIM4_Run()
{
    M0P_ADTIM4->SSTAR = (1<<0);
}

FORCE_IN_LINE void HC32TIM4_Stop()
{
    M0P_ADTIM4->SSTPR = (1<<0);
}

// 设置TIM4指定通道的比较值。如果初始化之后修改了是否启用通道缓存，本函数将不可用。可以使用 HC32ADTIM_SetCompValue。
// ch - 通道号，0~1 分别代表 A~B。
// value - 比较值。
FORCE_IN_LINE void HC32TIM4_SetCompValue(int ch, int value)
{
#if (TIM4_CHA_EN_BUFF)
    *(vu32 *)(M0P_ADTIM4_BASE + 0x10 + 8 + ch * 4) = value;
#else
    *(vu32 *)(M0P_ADTIM4_BASE + 0x10 + ch * 4) = value;
#endif
}

void HC32TIM5_Init();

STATIC_FORCE_IN_LINE void HC32TIM5_Run()
{
    M0P_ADTIM5->SSTAR = (1<<1);
}

STATIC_FORCE_IN_LINE void HC32TIM5_Stop()
{
    M0P_ADTIM5->SSTPR = (1<<1);
}

// 设置TIM5指定通道的比较值。如果初始化之后修改了是否启用通道缓存，本函数将不可用。可以使用 HC32ADTIM_SetCompValue。
// ch - 通道号，0~1 分别代表 A~B。
// value - 比较值。
FORCE_IN_LINE void HC32TIM5_SetCompValue(int ch, int value)
{
#if (TIM5_CHA_EN_BUFF)
    *(vu32 *)(M0P_ADTIM5_BASE + 0x10 + 8 + ch * 4) = value;
#else
    *(vu32 *)(M0P_ADTIM5_BASE + 0x10 + ch * 4) = value;
#endif
}

void HC32TIM6_Init();

STATIC_FORCE_IN_LINE void HC32TIM6_Run()
{
    M0P_ADTIM6->SSTAR = (1<<2);
}

STATIC_FORCE_IN_LINE void HC32TIM6_Stop()
{
    M0P_ADTIM6->SSTPR = (1<<2);
}

// 设置TIM6指定通道的比较值。如果初始化之后修改了是否启用通道缓存，本函数将不可用。可以使用 HC32ADTIM_SetCompValue。
// ch - 通道号，0~1 分别代表 A~B。
// value - 比较值。
FORCE_IN_LINE void HC32TIM6_SetCompValue(int ch, int value)
{
#if (TIM6_CHA_EN_BUFF)
    *(vu32 *)(M0P_ADTIM6_BASE + 0x10 + 8 + ch * 4) = value;
#else
    *(vu32 *)(M0P_ADTIM6_BASE + 0x10 + ch * 4) = value;
#endif
}

// 同时启动TIM4 TIM5 TIM6
// sb - 要同时启动的定时器，最后3位有效，[2..0]分别代表 TIM6 TIM5 TIM4
STATIC_FORCE_IN_LINE void HC32ADTIM_SyncRun(u16 sb)
{
    M0P_ADTIM4->SSTAR = sb;
}

// 同时停止TIM4 TIM5 TIM6
// sb - 要同时停止的定时器，最后3位有效，[2..0]分别代表 TIM6 TIM5 TIM4
STATIC_FORCE_IN_LINE void HC32ADTIM_SyncStop(u16 sb)
{
    M0P_ADTIM4->SSTPR = sb;
}

// 设置指定TIM、通道的比较值。[占用资源较多的API]
// adtim - 指定高级定时器。
// ch - 通道号，0~1 分别代表 A~B。
// value - 比较值。
void HC32ADTIM_SetCompValue(M0P_ADTIM_TypeDef * adtim, int ch, int value);

#ifdef __cplusplus
}
#endif

#endif
