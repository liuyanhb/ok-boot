/***********************************************************************************
 * 文件名： mcu_uid_lock.h
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		利用UID加密。依赖 flash 驱动。
 * 修改记录：
 * 	2020/8/19: 初版。 刘言。
***********************************************************************************/
#ifndef _MCU_UID_LOCK_H_
#define _MCU_UID_LOCK_H_

#include "typedef.h"
#include "HC32_LyLib.h"


#ifndef HC32_EXIST_CONFIG_FILE

#define UL_INIT_KEY		0x01234567

#endif


#define UID_ADDR	0x00100E74
#define UID_LEN		10


typedef struct _uid_pram_t
{
  u32 UidMatchCode;
}uid_pram_t;

#define UID_PRAM_DEFAULT        \
{                               \
  UL_INIT_KEY                   \
}     

// UID加密模块初始化，必须先调用 OcpFlash_Init 初始化片上FLASH
void ULock_Init();  

// UID加密验证，需要循环调用，如果验证不通过将会调用main函数。
void ULock_Verify();      

// 获取UID，需要的时候调用。
u8 * ULock_GetUid();      

#endif
