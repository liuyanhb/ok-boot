/***********************************************************************************
 * 文件名： HC32_LyLibConfig.h
 * 作者： 刘言
 * 版本： 4
 * 说明：
 * 		HC32 LyLib 统一配置文件。本文件作为 HC32_LyLib.h 的一部分，不要直接包含本文件。
***********************************************************************************/

#define HC32_EXIST_CONFIG_FILE              // 存在本配置文件

#define HC32_MCU_SERIES     HC32F005        // 型号系列


/////////////////////////////////////////////////////////////////////////////////////
// 外设的初始参数配置，调用相应的初始化函数后将按照以下参数进行初始化，如需运行过程中修改参数
// 调用对应驱动的API即可。对于不使用的外设（不调用Init函数）可以不必理会其配置参数。
/////////////////////////////////////////////////////////////////////////////////////

//////////////////////// FLASH 起始地址
// 本软件在FLASH中保存的起始地址
// 如果OTA升级(存在BootLoader)，用户程序起始地址可能不为0，必须修改实际的起始地址，
// 这会影响中断向量表地址寄存器（VTOR），和Flash的读写操作。
// 与实际不符会导致中断无法响应、跑飞！！！
#define FLASH_BASE          0

//////////////////////// 时钟配置

#define CLOCK_SOURCE		0		// 0:内部RC振荡器（F_SYS=F_HSI）；1：外部晶振(F_SYS=F_HSE)。
#define F_HSE				0		// 外部晶振频率，单位MHz，CLOCK_SOURCE==1时有效。
#define F_HSI	    		4    	// 内部RC振荡器（RCH）频率，可选：4 8 16 24 MHz
#define MCU_HCLK_PRS        2       // [0~7]HCLK（CPU与高速总线时钟）分频设置，F_HCLK = F_SYS/(2^HCLK_PRS);
#define MCU_PCLK_PRS        0       // [0~3]PCLK（外设总线时钟）分频设置，F_PCLK = F_HCLK/(2^PCLK_PRS);
// 外设时钟使能寄存器初始值，外设时钟使能初始状态，需要用到的外设可以在这里统一使能时钟门控，多个外设用 | 隔开。
#define PERI_CLKEN_INIT     FLASH_CLKEN|GPIO_CLKEN|BASETIM_CLKEN|UART1_CLKEN

//////////////////////// Timer 的配置

#if (HC32_MCU_SERIES == HC32F005)   // HC32F005 系列，其 TIM0 1 2 是基础定时器

#define TIM0_PRS            0       // 0~7,预分频：1,2,4,8,16,32,64,256。
#define TIM0_CYCLE  	  	1       // 16bits 或者 32bits 整数  设置定时器周期时钟数
#define TIM0_AUTO_RELOAD    0       // 1:自动重装载（16bits），0:不自动重装载(32bits)
#define TIM0_IE             0       // 1:开启中断,0:不开启中断
#define TIM0_RUN            1       // 1-初始化后立即运行，0-初始化后不运行

#define TIM1_PRS            0       // 0~7,预分频：1,2,4,8,16,32,64,256。
#define TIM1_CYCLE  	  	((F_PCLK*1000000UL*2)/(UART1_BAUD_RATE*32))    // 16bits 或者 32bits  设置定时器周期时钟数
#define TIM1_AUTO_RELOAD    1       // 1:自动重装载（16bits），0:不自动重装载(32bits)
#define TIM1_IE             0       // 1:开启中断,0:不开启中断
#define TIM1_RUN            1       // 1-初始化后立即运行，0-初始化后不运行

#define TIM2_PRS            7       // 0~7,预分频：1,2,4,8,16,32,64,256。
#define TIM2_CYCLE  	  	9375ul  // 16bits 或者 32bits 整数  设置定时器周期时钟数
#define TIM2_AUTO_RELOAD    1       // 1:自动重装载（16bits），0:不自动重装载(32bits)
#define TIM2_IE             1       // 1:开启中断,0:不开启中断
#define TIM2_RUN            1       // 1-初始化后立即运行，0-初始化后不运行

#else   // HC32F005 系列之外的MCU型号系列，其 TIM0 1 2 是通用定时器

// 以下为模式0的配置，目前本驱动只支持模式0

#define TIM0_PRS            0       // 0~7,预分频：PCLK / 1,2,4,8,16,32,64,256。
#define TIM0_CYCLE  	  	1       // 16bits 或者 32bits 整数  设置定时器周期时钟数
#define TIM0_AUTO_RELOAD    0       // 1:自动重装载（16bits），0:不自动重装载(32bits)
#define TIM0_UIE            0       // 1:开启中断,0:不开启中断

#define TIM1_PRS            0       // 0~7,预分频：PCLK / 1,2,4,8,16,32,64,256。
#define TIM1_CYCLE  	  	1       // 16bits 或者 32bits 整数  设置定时器周期时钟数
#define TIM1_AUTO_RELOAD    1       // 1:自动重装载（16bits），0:不自动重装载(32bits)
#define TIM1_UIE            0       // 1:开启中断,0:不开启中断

#define TIM2_PRS            7       // 0~7,预分频：PCLK / 1,2,4,8,16,32,64,256。
#define TIM2_CYCLE  	  	9375ul  // 16bits 或者 32bits 整数  设置定时器周期时钟数
#define TIM2_AUTO_RELOAD    1       // 1:自动重装载（16bits），0:不自动重装载(32bits)
#define TIM2_UIE            1       // 1:开启中断,0:不开启中断

#endif

// TIM4初始参数

#define TIM4_CMODE          0       // 计数模式：0-锯齿波A模式，4-三角波A模式，5-三角波B模式。不要设置其他值。
#define TIM4_CDIR           1       // 计数方向：0-递减，1-递加
#define TIM4_CKDIV          0       // 计数时钟分频：0~7分别代表1、2、4、8、16、64、256、1024分频
#define TIM4_CYCLE          44      // 计数周期值 1~65535
#define TIM4_DTCEN          0       // 1-死区功能有效，0-关闭
#define TIM4_SEPA           0       // 1-DTUA和DTDA(2个死区)的值自动相等，0-分别设置
#define TIM4_DTUA           0       // 向上计数时的死区时间 1~65535，GCMBR = GCMAR - DTUAR
#define TIM4_DTDA           0       // 向下计数时的死区时间 1~65535，GCMBR = GCMAR - DTDAR
#define TIM4_EN_INTA        0       // 1-使能通道A匹配或者捕获事件中断  
#define TIM4_EN_INTB        0       // 1-使能通道B匹配或者捕获事件中断  
#define TIM4_EN_INTOV       0       // 1-使能计数上溢中断  
#define TIM4_EN_INTUD       0       // 1-使能计数下溢中断  
#define TIM4_EN_INTDE       0       // 1-使能死区时间错误异常中断
#define TIM4_RUN            0       // 1-初始化后立即开始运行（计数），0-初始化后不运行

#define TIM4_CHA_VALUE      2       // 比较/捕获值 1~65535。（与计数值相等时输出翻转，三角波模式下PWM占空比=TIM4_CHA_VALUE/TIM4_CYCLE）
#define TIM4_CHA_EN_BUFF    1       // 1-启用缓存，仅在计数周期结束时更新新的值，0-不启用
#define TIM4_CHA_MODE       0       // 模式：0-比较输出模式，1-捕获输入模式
#define TIM4_CHA_EN_PORT    0       // 1-端口使能，0-失能
#define TIM4_CHA_EPA        2       // 计数值与周期值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM4_CHA_ECA        2       // 计数值与比较值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM4_CHA_SELM       0       // 计数开始或停止时（起始结束）电平模式：0-设置（指定）的电平，1-保持之前的电平不变化
#define TIM4_CHA_SL         0       // 起始（计数开始时）电平：0-低电平，1-高电平。TIM4_CHA_SELM = 1 时无效。
#define TIM4_CHA_EL         0       // 结束（计数停止时）电平：0-低电平，1-高电平。TIM4_CHA_SELM = 1 时无效。
#define TIM4_CHA_BKS        0       // 比较输出时刹车触发信号源：0-VC中断标志，1-AB通道电平相同，2-进入低功耗，3-端口或软件控制
#define TIM4_CHA_BKL        0       // 刹车电平状态：0-无动作（刹车无效），1-高阻，2-低电平，3-高电平
#define TIM4_CHA_EN_FILT    0       // 1-使能捕获输入滤波，0-不使用。（使能后连续3次检测到相同电平才有效）
#define TIM4_CHA_FILT_CK    0       // 滤波时钟分频：0-PCLK,1-PCLK/4,2-PCLK/16,3-PCLK/64

#define TIM4_CHB_VALUE      44      // 比较/捕获值 0~65535。（互补输出时启用死区设置后，该值无效）
#define TIM4_CHB_EN_BUFF    1       // 1-启用缓存，仅在计数周期结束时更新新的比较值，0-不启用 （互补输出时启用死区设置后，该值应保持0）
#define TIM4_CHB_MODE       0       // 模式：0-比较输出模式，1-捕获输入模式
#define TIM4_CHB_EN_PORT    1       // 1-端口使能，0-失能
#define TIM4_CHB_EPA        0       // 计数值与周期值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM4_CHB_ECA        1       // 计数值与比较值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM4_CHB_SELM       0       // 计数开始或停止时（起始结束）电平模式：0-设置（指定）的电平，1-保持之前的电平不变化
#define TIM4_CHB_SL         0       // 起始（计数开始时）电平：0-低电平，1-高电平。TIM4_CHA_SELM = 1 时无效。
#define TIM4_CHB_EL         0       // 结束（计数停止时）电平：0-低电平，1-高电平。TIM4_CHA_SELM = 1 时无效。
#define TIM4_CHB_BKS        0       // 比较输出时刹车触发信号源：0-VC中断标志，1-AB通道电平相同，2-进入低功耗，3-端口或软件控制
#define TIM4_CHB_BKL        2       // 刹车电平状态：0-无动作（刹车无效），1-高阻，2-低电平，3-高电平
#define TIM4_CHB_EN_FILT    0       // 1-使能捕获输入滤波，0-不使用。（使能后连续3次检测到相同电平才有效）
#define TIM4_CHB_FILT_CK    0       // 滤波采样时钟分频：0-PCLK,1-PCLK/4,2-PCLK/16,3-PCLK/64

// TIM5初始参数

#define TIM5_CMODE          4       // 计数模式：0-锯齿波A模式，4-三角波A模式，5-三角波B模式。不要设置其他值。
#define TIM5_CDIR           1       // 计数方向：0-递减，1-递加
#define TIM5_CKDIV          7       // 计数时钟分频：0~7分别代表1、2、4、8、16、64、256、1024分频
#define TIM5_CYCLE          10000   // 计数周期值 1~65535
#define TIM5_DTCEN          0       // 1-死区功能有效，0-关闭
#define TIM5_SEPA           0       // 1-DTUA和DTDA的值自动相等，0-分别设置
#define TIM5_DTUA           0       // 向上计数时的死区时间 1~65535，GCMBR = GCMAR - DTUAR
#define TIM5_DTDA           0       // 向下计数时的死区时间 1~65535，GCMBR = GCMAR - DTDAR
#define TIM5_EN_INTA        0       // 1-使能通道A匹配或者捕获事件中断  
#define TIM5_EN_INTB        0       // 1-使能通道B匹配或者捕获事件中断  
#define TIM5_EN_INTOV       0       // 1-使能计数上溢中断  
#define TIM5_EN_INTUD       0       // 1-使能计数下溢中断  
#define TIM5_EN_INTDE       0       // 1-使能死区时间错误异常中断
#define TIM5_RUN            0       // 1-初始化后立即开始运行（计数），0-初始化后不运行

#define TIM5_CHA_VALUE      5000    // 比较/捕获值 1~65535。（与计数值相等时输出翻转，三角波模式下PWM占空比=TIM5_CHA_VALUE/TIM5_CYCLE）
#define TIM5_CHA_EN_BUFF    1       // 1-启用缓存，仅在计数周期结束时更新新的值，0-不启用
#define TIM5_CHA_MODE       0       // 模式：0-比较输出模式，1-捕获输入模式
#define TIM5_CHA_EN_PORT    0       // 1-端口使能，0-失能
#define TIM5_CHA_EPA        2       // 计数值与周期值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM5_CHA_ECA        2       // 计数值与比较值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM5_CHA_SELM       0       // 计数开始或停止时（起始结束）电平模式：0-设置（指定）的电平，1-保持之前的电平不变化
#define TIM5_CHA_SL         0       // 起始（计数开始时）电平：0-低电平，1-高电平。TIM5_CHA_SELM = 1 时无效。
#define TIM5_CHA_EL         0       // 结束（计数停止时）电平：0-低电平，1-高电平。TIM5_CHA_SELM = 1 时无效。
#define TIM5_CHA_BKS        0       // 比较输出时刹车触发信号源：0-VC中断标志，1-AB通道电平相同，2-进入低功耗，3-端口或软件控制
#define TIM5_CHA_BKL        0       // 刹车电平状态：0-无动作（刹车无效），1-高阻，2-低电平，3-高电平
#define TIM5_CHA_EN_FILT    0       // 1-使能捕获输入滤波，0-不使用。（使能后连续3次检测到相同电平才有效）
#define TIM5_CHA_FILT_CK    0       // 滤波时钟分频：0-PCLK,1-PCLK/4,2-PCLK/16,3-PCLK/64

#define TIM5_CHB_VALUE      5000    // 比较/捕获值 1~65535。（互补输出时启用死区设置后，该值无效）
#define TIM5_CHB_EN_BUFF    1       // 1-启用缓存，仅在计数周期结束时更新新的比较值，0-不启用 （互补输出时启用死区设置后，该值应保持0）
#define TIM5_CHB_MODE       0       // 模式：0-比较输出模式，1-捕获输入模式
#define TIM5_CHB_EN_PORT    0       // 1-端口使能，0-失能
#define TIM5_CHB_EPA        2       // 计数值与周期值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM5_CHB_ECA        2       // 计数值与比较值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM5_CHB_SELM       0       // 计数开始或停止时（起始结束）电平模式：0-设置（指定）的电平，1-保持之前的电平不变化
#define TIM5_CHB_SL         0       // 起始（计数开始时）电平：0-低电平，1-高电平。TIM5_CHA_SELM = 1 时无效。
#define TIM5_CHB_EL         0       // 结束（计数停止时）电平：0-低电平，1-高电平。TIM5_CHA_SELM = 1 时无效。
#define TIM5_CHB_BKS        0       // 比较输出时刹车触发信号源：0-VC中断标志，1-AB通道电平相同，2-进入低功耗，3-端口或软件控制
#define TIM5_CHB_BKL        0       // 刹车电平状态：0-无动作（刹车无效），1-高阻，2-低电平，3-高电平
#define TIM5_CHB_EN_FILT    0       // 1-使能捕获输入滤波，0-不使用。（使能后连续3次检测到相同电平才有效）
#define TIM5_CHB_FILT_CK    0       // 滤波采样时钟分频：0-PCLK,1-PCLK/4,2-PCLK/16,3-PCLK/64

// TIM6初始参数

#define TIM6_CMODE          4       // 计数模式：0-锯齿波A模式，4-三角波A模式，5-三角波B模式。不要设置其他值。
#define TIM6_CDIR           1       // 计数方向：0-递减，1-递加
#define TIM6_CKDIV          0       // 计数时钟分频：0~7分别代表1、2、4、8、16、64、256、1024分频
#define TIM6_CYCLE          4       // 计数周期值 1~65535
#define TIM6_DTCEN          1       // 1-死区功能有效，0-关闭
#define TIM6_SEPA           0       // 1-DTUA和DTDA的值自动相等，0-分别设置
#define TIM6_DTUA           0       // 向上计数时的死区时间 1~65535，GCMBR = GCMAR - DTUAR
#define TIM6_DTDA           0       // 向下计数时的死区时间 1~65535，GCMBR = GCMAR - DTDAR
#define TIM6_EN_INTA        0       // 1-使能通道A匹配或者捕获事件中断  
#define TIM6_EN_INTB        0       // 1-使能通道B匹配或者捕获事件中断  
#define TIM6_EN_INTOV       0       // 1-使能计数上溢中断  
#define TIM6_EN_INTUD       0       // 1-使能计数下溢中断  
#define TIM6_EN_INTDE       0       // 1-使能死区时间错误异常中断
#define TIM6_RUN            0       // 1-初始化后立即开始运行（计数），0-初始化后不运行

#define TIM6_CHA_VALUE      2       // 比较/捕获值 1~65535。（与计数值相等时输出翻转，三角波模式下PWM占空比=TIM6_CHA_VALUE/TIM6_CYCLE）
#define TIM6_CHA_EN_BUFF    1       // 1-启用缓存，仅在计数周期结束时更新新的值，0-不启用
#define TIM6_CHA_MODE       0       // 模式：0-比较输出模式，1-捕获输入模式
#define TIM6_CHA_EN_PORT    1       // 1-端口使能，0-失能
#define TIM6_CHA_EPA        2       // 计数值与周期值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM6_CHA_ECA        3       // 计数值与比较值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM6_CHA_SELM       0       // 计数开始或停止时（起始结束）电平模式：0-设置（指定）的电平，1-保持之前的电平不变化
#define TIM6_CHA_SL         0       // 起始（计数开始时）电平：0-低电平，1-高电平。TIM6_CHA_SELM = 1 时无效。
#define TIM6_CHA_EL         0       // 结束（计数停止时）电平：0-低电平，1-高电平。TIM6_CHA_SELM = 1 时无效。
#define TIM6_CHA_BKS        0       // 比较输出时刹车触发信号源：0-VC中断标志，1-AB通道电平相同，2-进入低功耗，3-端口或软件控制
#define TIM6_CHA_BKL        0       // 刹车电平状态：0-无动作（刹车无效），1-高阻，2-低电平，3-高电平
#define TIM6_CHA_EN_FILT    0       // 1-使能捕获输入滤波，0-不使用。（使能后连续3次检测到相同电平才有效）
#define TIM6_CHA_FILT_CK    0       // 滤波时钟分频：0-PCLK,1-PCLK/4,2-PCLK/16,3-PCLK/64

#define TIM6_CHB_VALUE      2       // 比较/捕获值 1~65535。（互补输出时启用死区设置后，该值无效）
#define TIM6_CHB_EN_BUFF    0       // 1-启用缓存，仅在计数周期结束时更新新的比较值，0-不启用 （互补输出时启用死区设置后，该值应保持0）
#define TIM6_CHB_MODE       0       // 模式：0-比较输出模式，1-捕获输入模式
#define TIM6_CHB_EN_PORT    1       // 1-端口使能，0-失能
#define TIM6_CHB_EPA        2       // 计数值与周期值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM6_CHB_ECA        3       // 计数值与比较值相等时，动作：0-低电平，1-高电平，2-无动作，3-翻转
#define TIM6_CHB_SELM       0       // 计数开始或停止时（起始结束）电平模式：0-设置（指定）的电平，1-保持之前的电平不变化
#define TIM6_CHB_SL         1       // 起始（计数开始时）电平：0-低电平，1-高电平。TIM6_CHA_SELM = 1 时无效。
#define TIM6_CHB_EL         0       // 结束（计数停止时）电平：0-低电平，1-高电平。TIM6_CHA_SELM = 1 时无效。
#define TIM6_CHB_BKS        0       // 比较输出时刹车触发信号源：0-VC中断标志，1-AB通道电平相同，2-进入低功耗，3-端口或软件控制
#define TIM6_CHB_BKL        0       // 刹车电平状态：0-无动作（刹车无效），1-高阻，2-低电平，3-高电平
#define TIM6_CHB_EN_FILT    0       // 1-使能捕获输入滤波，0-不使用。（使能后连续3次检测到相同电平才有效）
#define TIM6_CHB_FILT_CK    0       // 滤波采样时钟分频：0-PCLK,1-PCLK/4,2-PCLK/16,3-PCLK/64

//////////////////////// UART 的配置

#define UART0_BAUD_RATE     9600        // 初始波特率 
#define UART0_RIEN          0           // 1：开启接收中断；0：不开启
#define _UART0_USE_HALF_DUPLEX           // 定义表示使用半双工模式，不定义使用默认的全双工模式
    #define _UART0_PIN_TX_MODE      // IO口切换成发送模式。当UART支持硬件半双工自动切换收发时定义为空。
    #define _UART0_PIN_RX_MODE      // IO口切换成接收模式。当UART支持硬件半双工自动切换收发时定义为空。

#define UART1_BAUD_RATE     62500        // 初始波特率
#define UART1_RIEN          0           // 1：开启接收中断；0：不开启
#define _UART1_USE_HALF_DUPLEX           // 定义表示使用半双工模式，不定义使用默认的全双工模式
    #define _UART1_PIN_TX_MODE  GPIO_P01_SEL_UART1_TXD;M0P_GPIO->P0DIR = 0XFFFFFFFD/*GPIO_DIR_OUTPUT(0, 1)*/;GPIO_P02_SEL_GPIO_P02;   // IO口切换成发送模式。当UART支持硬件半双工自动切换收发时定义为空。
    #define _UART1_PIN_RX_MODE  M0P_GPIO->P0DIR = 0XFFFFFFFF/*GPIO_DIR_INPUT(0, 1)*/;GPIO_P01_SEL_GPIO_P01;GPIO_P02_SEL_UART1_RXD;   // IO口切换成接收模式。当UART支持硬件半双工自动切换收发时定义为空。

//////////////////////// FLASH 的配置

#define FLASH_PAGE_SIZE     512
#define FLASH_SIZE          (16 * 1024)
#define FLASH_UNLOCK_VALUE  0X0000FFFF      // FLASH解锁时SLOCK寄存器的值

//////////////////////// UID_LOCK 的配置

#define UL_INIT_KEY		0xA1234567

/////////////////////// Delay的配置

#define DELAY_TIM_INIT                          // Delay 所使用的定时器初始化，如果在别处初始化了这里留空。
#define DELAY_COUNTER           SysTick->VAL    // 计数寄存器，默认是递减计数。
#define DELAY_COUNT_PERIOD      (1000 * F_HCLK) // 计数周期（重载值）,这里不是设置，只是告知延时驱动定时器的重载值。
#define DELAY_COUNT_PER_US      F_HCLK          // 1us 的计数个数 （计数的频率，单位MHz. SysTick会被库函数设置为HCLK）

/////////////////////// ADC 初始参数配置

#define ADC_CLKSEL      3   // 0~3 ADC时钟分频 = 2^ADC_CLKSEL , FADC = PCLK/2^ADC_CLKSEL = 24/8=3MHz
#define ADC_SAM         3   // 0~3 ADC采样周期：4,6,8,12，ADC转换周期 = 采样周期 + 16 = 20 个时钟周期,150kHz 6.667us 
#define ADC_SREF        3   // 0~3 ADC参考电压：内部1.5V，内部2.5V，外部P36，电源电压
#define ADC_BUFEN       1   // 0 关闭，1 开启 信号放大器（电压跟随器）

#if (HC32_MCU_SERIES == HC32F005)   // 005专用的配置项

#define ADC_SCAN_CNT            3       // 0~7 ADC扫描模式转换次数，大于等于扫描模式开启的通道数，=0不使用扫描功能（可节省代码量）
#define ADC_SCAN_CHEN_MASK      0x23    // 0010 0011 。扫描模式开启的通道，1表示开启。
#define ADC_TIMING_SCAN         1       // 1：启用定时扫描功能，依据所使用的定时器进行下列项目的配置
    #define ADC_DISALLOW_INT_START_SCAN     Tim1_DisableInt()   // 杜绝在中断中启动扫描转换的可能性
    #define ADC_ALLOW_INT_START_SCAN        Tim1_EnableInt()    // 允许在中断中启动扫描转换
    #define ADC_START_TIMING_SCAN           Tim1_Run()          // 开始定时扫描
    #define ADC_STOP_TIMING_SCAN            Tim1_Stop()         // 停止定时扫描

#endif
/////////////////////// PCA 初始参数配置

#define _PCA_CIDL       0       // 1-休眠后停止工作，0-休眠后继续工作
#define _PCA_CPS        0       // 时钟选择：0-PCLK/32,1-PCLK/16,2-PCLK/8,3-PCLK/4,4-PCLK/2,5-T0,6-T1,7-ECI外部时钟
#define _PCA_CFIE       0       // 1-使能计数溢出中断
#define _PCA_RUN        1       // 1-初始化后立即运行

// ECOM     CAPP    CAPN    MAT     TOG     PWM     工作方式
// X        1       0       0       0       0       用正沿触发捕获
// X        0       1       0       0       0       用负沿触发捕获
// X        1       1       0       0       0       用跳变沿触发捕获
// 1        0       0       1       0       0       软件定时器
// 1        0       0       1       1       0       高速输出
// 1        0       0       0       0       1       8位脉冲宽度调制器

#define _PCA_CH0_CCIE   0       // 1-使能比较/捕获中断
#define _PCA_CH0_PWM    1       // 1-开启PWM功能，固定8位计数，PWM值范围0~255，计数值小于PWM值输出L，大于等于输出H
#define _PCA_CH0_TOG    0       // 1-开启匹配引脚翻转功能，计数值与比较值相等时输出翻转
#define _PCA_CH0_MAT    0       // 1-启用匹配功能
#define _PCA_CH0_CAPN   0       // 1-开启下降沿捕获
#define _PCA_CH0_CAPP   0       // 1-开启上升沿捕获
#define _PCA_CH0_ECOM   1       // 1-开启比较功能（比较输出必须开启，捕获功能无需开启）

#define _PCA_CH1_CCIE   0       // 1-使能比较/捕获中断
#define _PCA_CH1_PWM    1       // 1-开启PWM功能，固定8位计数，PWM值范围0~255，计数值小于PWM值输出L，大于等于输出H
#define _PCA_CH1_TOG    0       // 1-开启匹配引脚翻转功能，计数值与比较值相等时输出翻转
#define _PCA_CH1_MAT    0       // 1-启用匹配功能
#define _PCA_CH1_CAPN   0       // 1-开启下降沿捕获
#define _PCA_CH1_CAPP   0       // 1-开启上升沿捕获
#define _PCA_CH1_ECOM   1       // 1-开启比较功能（比较输出必须开启，捕获功能无需开启）

#define _PCA_CH2_CCIE   0       // 1-使能比较/捕获中断
#define _PCA_CH2_PWM    1       // 1-开启PWM功能，固定8位计数，PWM值范围0~255，计数值小于PWM值输出L，大于等于输出H
#define _PCA_CH2_TOG    0       // 1-开启匹配引脚翻转功能，计数值与比较值相等时输出翻转
#define _PCA_CH2_MAT    0       // 1-启用匹配功能
#define _PCA_CH2_CAPN   0       // 1-开启下降沿捕获
#define _PCA_CH2_CAPP   0       // 1-开启上升沿捕获
#define _PCA_CH2_ECOM   1       // 1-开启比较功能（比较输出必须开启，捕获功能无需开启）

#define _PCA_CH3_CCIE   0       // 1-使能比较/捕获中断
#define _PCA_CH3_PWM    0       // 1-开启PWM功能，固定8位计数，PWM值范围0~255，计数值小于PWM值输出L，大于等于输出H
#define _PCA_CH3_TOG    0       // 1-开启匹配引脚翻转功能，计数值与比较值相等时输出翻转
#define _PCA_CH3_MAT    0       // 1-启用匹配功能
#define _PCA_CH3_CAPN   0       // 1-开启下降沿捕获
#define _PCA_CH3_CAPP   0       // 1-开启上升沿捕获
#define _PCA_CH3_ECOM   0       // 1-开启比较功能（比较输出必须开启，捕获功能无需开启）

#define _PCA_CH4_CCIE   0       // 1-使能比较/捕获中断
#define _PCA_CH4_PWM    0       // 1-开启PWM功能，固定8位计数，PWM值范围0~255，计数值小于PWM值输出L，大于等于输出H
#define _PCA_CH4_TOG    0       // 1-开启匹配引脚翻转功能，计数值与比较值相等时输出翻转
#define _PCA_CH4_MAT    0       // 1-启用匹配功能
#define _PCA_CH4_CAPN   0       // 1-开启下降沿捕获
#define _PCA_CH4_CAPP   0       // 1-开启上升沿捕获
#define _PCA_CH4_ECOM   0       // 1-开启比较功能（比较输出必须开启，捕获功能无需开启）




