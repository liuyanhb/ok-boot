/***********************************************************************************
 * 文件名： hc32_general_uart.h
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		通用 UART 驱动。注意，HC32F005系列的UART是基本UART，见 hc32_base_uart.h。
 *      中断入口统一定义在 isr.c 中。
 * 修改记录：
 * 	2020/8/19: 初版。 刘言。
***********************************************************************************/
#ifndef _HC32_GENERAL_UART_H_
#define _HC32_GENERAL_UART_H_

#ifdef __cplusplus
 extern "C" {
#endif


#if (HC32_MCU_SERIES != HC32F005)


#include "mcu.h"


#define UART_PCLK    (F_PCLK*1000000ul)      // UART_PCLK 初始频率 HZ  

#ifndef HC32_EXIST_CONFIG_FILE

#define UART1_BAUD_RATE     9600        // 初始波特率 
#define UART1_RIEN          0           // 1：开启接收中断；0：不开启

#define UART0_BAUD_RATE     9600        // 初始波特率 
#define UART0_RIEN          0           // 1：开启接收中断；0：不开启

#endif


void UART0_Init();
void UART0_SendByte(u8 c);
void UART0_Send(const u8 *buff, u16 Length);
void UART0_SetBaud(u32 baud);
#define UART0_GetRecivedFlag()       M0P_UART0->ISR_f.RC
#define UART0_ClearRecivedFlag()     M0P_UART0->ICR_f.RCCF = 0
#define UART0_GetRecivedbyte()       M0P_UART0->SBUF

void UART1_Init();
void UART1_SendByte(u8 c);
void UART1_Send(const u8 *buff, u16 Length);
void UART1_SetBaud(u32 baud);
#define UART1_GetRecivedFlag()       M0P_UART1->ISR_f.RC
#define UART1_ClearRecivedFlag()     M0P_UART1->ICR_f.RCCF = 0
#define UART1_GetRecivedbyte()       M0P_UART1->SBUF


#endif


#ifdef __cplusplus
}
#endif


#endif
