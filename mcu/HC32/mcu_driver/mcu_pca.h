/***********************************************************************************
 * 文件名： mcu_pca.c
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		PCA(可编程计数阵列)驱动。
 * 修改记录：
 * 	2020/12/25: 初版。 刘言。
***********************************************************************************/
#ifndef _MCU_PCA_H_
#define _MCU_PCA_H_

#include "HC32_LyLib.h"	

#ifndef HC32_EXIST_CONFIG_FILE

#define _PCA_CIDL       0       // 1-休眠后停止工作，0-休眠后继续工作
#define _PCA_CPS        0       // 时钟选择：0-PCLK/32,1-PCLK/16,2-PCLK/8,3-PCLK/4,4-PCLK/2,5-T0,6-T1,7-ECI外部时钟
#define _PCA_CFIE       0       // 1-使能计数溢出中断
#define _PCA_RUN        1       // 1-初始化后立即运行

// ECOM     CAPP    CAPN    MAT     TOG     PWM     工作方式
// X        1       0       0       0       0       用正沿触发捕获
// X        0       1       0       0       0       用负沿触发捕获
// X        1       1       0       0       0       用跳变沿触发捕获
// 1        0       0       1       0       0       软件定时器
// 1        0       0       1       1       0       高速输出
// 1        0       0       0       0       1       8位脉冲宽度调制器

#define _PCA_CH0_CCIE   0       // 1-使能比较/捕获中断
#define _PCA_CH0_PWM    1       // 1-开启PWM功能，固定8位计数，PWM值范围0~255，计数值小于PWM值输出L，大于等于输出H
#define _PCA_CH0_TOG    0       // 1-开启匹配引脚翻转功能，计数值与比较值相等时输出翻转
#define _PCA_CH0_MAT    0       // 1-启用匹配功能
#define _PCA_CH0_CAPN   0       // 1-开启下降沿捕获
#define _PCA_CH0_CAPP   0       // 1-开启上升沿捕获
#define _PCA_CH0_ECOM   1       // 1-开启比较功能（比较输出必须开启，捕获功能无需开启）

#define _PCA_CH1_CCIE   0       // 1-使能比较/捕获中断
#define _PCA_CH1_PWM    1       // 1-开启PWM功能，固定8位计数，PWM值范围0~255，计数值小于PWM值输出L，大于等于输出H
#define _PCA_CH1_TOG    0       // 1-开启匹配引脚翻转功能，计数值与比较值相等时输出翻转
#define _PCA_CH1_MAT    0       // 1-启用匹配功能
#define _PCA_CH1_CAPN   0       // 1-开启下降沿捕获
#define _PCA_CH1_CAPP   0       // 1-开启上升沿捕获
#define _PCA_CH1_ECOM   1       // 1-开启比较功能（比较输出必须开启，捕获功能无需开启）

#define _PCA_CH2_CCIE   0       // 1-使能比较/捕获中断
#define _PCA_CH2_PWM    1       // 1-开启PWM功能，固定8位计数，PWM值范围0~255，计数值小于PWM值输出L，大于等于输出H
#define _PCA_CH2_TOG    0       // 1-开启匹配引脚翻转功能，计数值与比较值相等时输出翻转
#define _PCA_CH2_MAT    0       // 1-启用匹配功能
#define _PCA_CH2_CAPN   0       // 1-开启下降沿捕获
#define _PCA_CH2_CAPP   0       // 1-开启上升沿捕获
#define _PCA_CH2_ECOM   1       // 1-开启比较功能（比较输出必须开启，捕获功能无需开启）

#define _PCA_CH3_CCIE   0       // 1-使能比较/捕获中断
#define _PCA_CH3_PWM    0       // 1-开启PWM功能，固定8位计数，PWM值范围0~255，计数值小于PWM值输出L，大于等于输出H
#define _PCA_CH3_TOG    0       // 1-开启匹配引脚翻转功能，计数值与比较值相等时输出翻转
#define _PCA_CH3_MAT    0       // 1-启用匹配功能
#define _PCA_CH3_CAPN   0       // 1-开启下降沿捕获
#define _PCA_CH3_CAPP   0       // 1-开启上升沿捕获
#define _PCA_CH3_ECOM   0       // 1-开启比较功能（比较输出必须开启，捕获功能无需开启）

#define _PCA_CH4_CCIE   0       // 1-使能比较/捕获中断
#define _PCA_CH4_PWM    0       // 1-开启PWM功能，固定8位计数，PWM值范围0~255，计数值小于PWM值输出L，大于等于输出H
#define _PCA_CH4_TOG    0       // 1-开启匹配引脚翻转功能，计数值与比较值相等时输出翻转
#define _PCA_CH4_MAT    0       // 1-启用匹配功能
#define _PCA_CH4_CAPN   0       // 1-开启下降沿捕获
#define _PCA_CH4_CAPP   0       // 1-开启上升沿捕获
#define _PCA_CH4_ECOM   0       // 1-开启比较功能（比较输出必须开启，捕获功能无需开启）

#endif

void Pca_Init();

STATIC_FORCE_IN_LINE void Pca_SetValue(int ch, int value)
{
    *(vu32 *)(M0P_PCA_BASE + 0x50 + ch * 4) = value;
}

STATIC_FORCE_IN_LINE int Pca_GetValue(int ch)
{
    return *(vu32 *)(M0P_PCA_BASE + 0x50 + ch * 4);
}

STATIC_FORCE_IN_LINE void Pca_SetPWMValue(int ch, int value)
{
    *(vu32 *)(M0P_PCA_BASE + 0x24 + ch * 8) = value;
}

STATIC_FORCE_IN_LINE int Pca_GetPWMValue(int ch)
{
    return *(vu32 *)(M0P_PCA_BASE + 0x24 + ch * 8);
}

#endif


