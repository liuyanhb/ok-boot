/***********************************************************************************
 * 文件名： hc32_base_uart.h
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		仅 HC32F005 系列拥有 base uart，它们是 UART0 UART1 ，其它型号MCU请使用 
 *  hc32_general_uart.h 。
 *      中断入口统一定义在 isr.c 中。
 * 修改记录：
 * 	2020/8/18: 初版。 刘言。
***********************************************************************************/
#ifndef _HC32_BASE_UART_H_
#define _HC32_BASE_UART_H_

#ifdef __cplusplus
 extern "C" {
#endif


#if (HC32_MCU_SERIES == HC32F005)


#include "mcu.h"


#define UART_PCLK    (F_PCLK*1000000UL)      // UART_PCLK 初始频率 HZ  

#ifndef HC32_EXIST_CONFIG_FILE

#define UART0_BAUD_RATE     9600        // 初始波特率 
#define UART0_RIEN          0           // 1：开启接收中断；0：不开启
#define _UART0_USE_HALF_DUPLEX           // 定义表示使用半双工模式，不定义使用默认的全双工模式
    #define _UART0_PIN_TX_MODE  WIRE_PIN_UART_TX_MODE   // IO口切换成发送模式。当UART支持硬件半双工自动切换收发时定义为空。
    #define _UART0_PIN_RX_MODE  WIRE_PIN_UART_RX_MODE   // IO口切换成接收模式。当UART支持硬件半双工自动切换收发时定义为空。

#define UART1_BAUD_RATE     9600        // 初始波特率 
#define UART1_RIEN          0           // 1：开启接收中断；0：不开启
#define _UART1_USE_HALF_DUPLEX           // 定义表示使用半双工模式，不定义使用默认的全双工模式
    #define _UART1_PIN_TX_MODE  WIRE_PIN_UART_TX_MODE   // IO口切换成发送模式。当UART支持硬件半双工自动切换收发时定义为空。
    #define _UART1_PIN_RX_MODE  WIRE_PIN_UART_RX_MODE   // IO口切换成接收模式。当UART支持硬件半双工自动切换收发时定义为空。
#endif



void UART0_Init();
void UART0_SendByte(u8 c);
void UART0_Send(const u8 *buff, u16 Length);
void UART0_SetBaud(u32 baud);
#define UART0_GetRecivedFlag()       M0P_UART0->ISR_f.RI
#define UART0_ClearRecivedFlag()     M0P_UART0->ICR_f.RICLR = 0
#define UART0_GetRecivedbyte()       M0P_UART0->SBUF

void UART1_Init();
void UART1_SendByte(u8 c);
void UART1_Send(const u8 *buff, u16 Length);
void UART1_SetBaud(u32 baud);
#define UART1_GetRecivedFlag()       M0P_UART1->ISR_f.RI
#define UART1_ClearRecivedFlag()     M0P_UART1->ICR_f.RICLR = 0
#define UART1_GetRecivedbyte()       M0P_UART1->SBUF


#endif


#ifdef __cplusplus
}
#endif


#endif
