/***********************************************************************************
 * 文件名： mcu.h
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		MCU 所有的驱动顶层文件，使用MCU驱动只用包含这一个文件即可。
 * 修改记录：
 * 	2020/8/18: 初版。 刘言。
***********************************************************************************/
#ifndef _MCU_H_
#define _MCU_H_

#ifdef __cplusplus
 extern "C" {
#endif

#include "HC32_LyLib.h"

#ifndef HC32_EXIST_CONFIG_FILE

//////////////////////// FLASH 起始地址
// 本软件在FLASH中保存的起始地址
// 如果OTA升级(存在BootLoader)，用户程序起始地址可能不为0，必须修改实际的起始地址，
// 这会影响中断向量表地址寄存器（VTOR），和Flash的读写操作。
// 与实际不符会导致中断无法响应、跑飞！！！
#define FLASH_BASE         0


//////////////////////// 时钟配置

#define CLOCK_SOURCE		0		// 0:内部RC振荡器；1：外部晶振。
#define F_HSE				0		// 外部晶振频率，单位MHz，CLOCK_SOURCE==1时有效。
#define F_HSI	    		24    	// 内部RC振荡器（RCH）频率，可选：4 8 16 24 MHz
#define MCU_HCLK_PRS            0       // [0~7]HCLK（高速总线时钟）分频设置，F_HCLK = F_SYS/2^HCLK_PRS;
#define MCU_PCLK_PRS            0       // [0~3]PCLK（外设总线时钟）分频设置，F_PCLK = F_HCLK/2^PCLK_PRS;

// 外设时钟使能寄存器初始值，外设时钟使能初始状态，需要用到的外设可以在这里统一使能时钟门控，多个外设用 | 隔开。
#define PERI_CLKEN_INIT     ALL_CLKEN //FLASH_CLKEN|GPIO_CLKEN|BASETIM_CLKEN|UART1_CLKEN

#endif

///////////////////////////// 参数检查

#if (HC32_MCU_SERIES == HC32F005)
	#if (F_HSI > 24)
		#error "HC32F005系列F_HSI最大24MHz."
	#elif (F_HSI < 4)
		#error "HC32F005系列F_HSI最小4MHz."
	#endif
#endif

///////////////////////////// 参数计算

#if (CLOCK_SOURCE==0)	//使用内部高速RC
	#define F_SYS	    F_HSI    	
#else					//使用外部晶振
	#define F_SYS		F_HSE	
#endif
#if (F_SYS == 0)
	#error "F_SYS不可以为0。"
#else
	#if (MCU_HCLK_PRS == 0)
		#define F_HCLK		F_SYS
	#elif (MCU_HCLK_PRS == 1)
		#define F_HCLK		(F_SYS/2)
	#elif (MCU_HCLK_PRS == 2)
		#define F_HCLK		(F_SYS/4)
	#elif (MCU_HCLK_PRS == 3)
		#define F_HCLK		(F_SYS/8)
	#elif (MCU_HCLK_PRS == 4)
		#define F_HCLK		(F_SYS/16)
	#elif (MCU_HCLK_PRS == 5)
		#define F_HCLK		(F_SYS/32)
	#elif (MCU_HCLK_PRS == 6)
		#define F_HCLK		(F_SYS/64)
	#elif (MCU_HCLK_PRS == 7)
		#define F_HCLK		(F_SYS/128)
	#endif
	#if (MCU_PCLK_PRS == 0)
		#define F_PCLK		F_HCLK
	#elif (MCU_PCLK_PRS == 1)
		#define F_PCLK		(F_HCLK/2)
	#elif (MCU_PCLK_PRS == 2)
		#define F_PCLK		(F_HCLK/4)
	#elif (MCU_PCLK_PRS == 3)
		#define F_PCLK		(F_HCLK/8)
	#endif
	#define F_CPU		  	F_HCLK
#endif

///////////////////////////// 提供的API

void SystemDeInit(void);

#define RCH_TRIM_24MHZ		*((volatile uint16_t *)0x00100C00ul)
#define RCH_TRIM_22_12MHZ	*((volatile uint16_t *)0x00100C02ul)
#define RCH_TRIM_16MHZ		*((volatile uint16_t *)0x00100C04ul)
#define RCH_TRIM_8MHZ		*((volatile uint16_t *)0x00100C06ul)
#define RCH_TRIM_4MHZ		*((volatile uint16_t *)0x00100C08ul)

// 设置RCH（HSI）频率校准值，注意前后2次的差值不能超过70！设置有死机风险！
// 可用于程序运行中调节RCH（HSI）频率
#define SystemSetRchTrim(a)	M0P_SYSCTRL->RCH_CR = a;

///////////////////////////// 外设驱动

#include "mcu_timer.h"
#include "mcu_uart.h"
#include "mcu_onchip_flash.h"
#include "mcu_uid_lock.h"
#include "mcu_power.h"
#include "mcu_delay.h"
#include "mcu_adc.h"
#include "mcu_aes.h"
#include "mcu_crc.h"
#include "mcu_temp_sensor.h"
#include "mcu_pca.h"

#ifdef __cplusplus
}
#endif

#endif

