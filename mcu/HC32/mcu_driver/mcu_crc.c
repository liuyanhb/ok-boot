/***********************************************************************************
 * 文件名： mcu_crc.c
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		CRC校验模块驱动。目前仅支持CRC32。
 * 修改记录：
 * 	2020/8/25: 初版。 刘言。
***********************************************************************************/
#include "HC32_LyLib.h"	
#include "mcu_crc.h"

// CRC32校验，校验码在数组最后4个字节（小端排列）
bool Crc_Verify32(const u8 *buff, u32 len)
{
    u32 i;

    M0P_CRC->CR_f.CR = 1;       // CRC32
    M0P_CRC->RESULT = 0XFFFFFFFF;
    for (i = 0; i < len; i++)
	{
        *((volatile uint8_t*)(&(M0P_CRC->DATA))) = buff[i];
	}
	return M0P_CRC->CR_f.FLAG;
}


