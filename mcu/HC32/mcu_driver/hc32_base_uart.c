/***********************************************************************************
 * 文件名： hc32_base_uart.c
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		仅 HC32F005 系列拥有 base uart，它们是 UART0 UART1 ，其它型号MCU请使用 
 *  hc32_general_uart.c 。
 *      中断入口统一定义在 isr.c 中。
 * 修改记录：
 * 	2020/8/18: 初版。 刘言。
***********************************************************************************/
#include "HC32_LyLib.h"
#include "hc32_base_uart.h"		

#if (HC32_MCU_SERIES == HC32F005)

void HC32TIM0_Init();
void HC32TIM1_Init();

void UART0_Init()
{
    // 端口设置,建议在别处初始化端口

    // 时钟门控
    // SYSCTRL_ENABLE_UART0_CLK; 已统一开启

    // 初始化 TIM0 产生波特率
    HC32TIM0_Init(); 

    // UART0 具体设置
    M0P_UART0->SCON =   (1 << 9)   // 双倍波特率
                      | (1 << 6) // 模式1 ： 1起始位，8数据位，1停止位； 模式3： 1起始位，8数据位，2停止位
                      | (1 << 4) // 使能收发
                      | (UART0_RIEN << 0) // 失能接收中断
        ;
    M0P_UART0->ICR = 0; // 清除标志位

#if (UART0_RIEN == 1)
    NVIC_ClearPendingIRQ(UART0_IRQn); // 清除NVIC中断标志位
    NVIC_SetPriority(UART0_IRQn, 3);  // 优先级 3 （0~3）
    NVIC_EnableIRQ(UART0_IRQn);       // NVIC允许中断
#endif
}

// 查询模式发送一个字节
void UART0_SendByte(u8 c)
{
#ifdef _UART0_USE_HALF_DUPLEX
    _UART0_PIN_TX_MODE;
#endif
    M0P_UART0->SBUF = c;
    while(M0P_UART0->ISR_f.TI == 0);
    M0P_UART0->ICR_f.TICLR = 0;
#ifdef _UART0_USE_HALF_DUPLEX
    _UART0_PIN_RX_MODE;
#endif
}

// 发送一串数据
void UART0_Send(const u8 *buff, u16 Length)
{
    while(Length--)
	{
		UART0_SendByte(*buff);
		buff++;
	}
}

// 设置波特率
void UART0_SetBaud(u32 baud)
{
    M0P_TIM0->ARR = 0x10000-((UART_PCLK*1000000UL*2)/(baud*32));       // 重载值
}


void UART1_Init()
{
    // 端口设置
    // GPIO_DIR_OUTPUT(3, 5);
    // GPIO_P35_SEL_UART1_TXD;
    // //GPIO_DIR_INPUT(3, 6);
    // GPIO_P36_SEL_UART1_RXD; 建议在别处初始化端口

    // 时钟门控
    // SYSCTRL_ENABLE_UART1_CLK; 已统一开启

    // 初始化 TIM1 产生波特率
    HC32TIM1_Init(); 

    // UART1 具体设置
    M0P_UART1->SCON =   (1 << 9)   // 双倍波特率
                      | (1 << 6) // 模式1 ： 1起始位，8数据位，1停止位； 模式3： 1起始位，8数据位，2停止位
                      | (1 << 4) // 使能收发
                      | (UART1_RIEN << 0) // 失能接收中断
        ;
    M0P_UART1->ICR = 0; // 清除标志位

#if (UART1_RIEN == 1)
    NVIC_ClearPendingIRQ(UART1_IRQn); // 清除NVIC中断标志位
    NVIC_SetPriority(UART1_IRQn, 3);  // 优先级 3 （0~3）
    NVIC_EnableIRQ(UART1_IRQn);       // NVIC允许中断
#endif
}

// 查询模式发送一个字节
void UART1_SendByte(u8 c)
{
#ifdef _UART1_USE_HALF_DUPLEX
    _UART1_PIN_TX_MODE;
#endif
    M0P_UART1->SBUF = c;
    while(M0P_UART1->ISR_f.TI == 0);
    M0P_UART1->ICR_f.TICLR = 0;
#ifdef _UART1_USE_HALF_DUPLEX
    _UART1_PIN_RX_MODE;
#endif
}

// 发送一串数据
void UART1_Send(const u8 *buff, u16 Length)
{
    while(Length--)
	{
		UART1_SendByte(*buff);
		buff++;
	}
}

// 设置波特率
void UART1_SetBaud(u32 baud)
{
    M0P_TIM1->ARR = 0x10000-((UART_PCLK*1000000UL*2)/(baud*32));       // 重载值
}


#endif

