/***********************************************************************************
 * 文件名： hc32_base_timer.h
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		仅 HC32F005 系列拥有 base timer，它们是 TIM0 TIM1 TIM2，其它型号MCU请使用 
 *  hc32_general_timer.c 。
 *      中断入口统一定义在 isr.c 中。
 * 修改记录：
 * 	2020/8/18: 初版。 刘言。
***********************************************************************************/
#ifndef _HC32_BASE_TIMER_H_
#define _HC32_BASE_TIMER_H_

#ifdef __cplusplus
 extern "C" {
#endif

#if (HC32_MCU_SERIES == HC32F005)


#ifndef HC32_EXIST_CONFIG_FILE

#include "hc32_base_uart.h"

#define TIM0_PRS            0       // 0~7,预分频：1,2,4,8,16,32,64,256。
#define TIM0_CYCLE  	  	1       // 16bits 或者 32bits 整数  设置定时器周期时钟数
#define TIM0_AUTO_RELOAD    0       // 1:自动重装载（16bits），0:不自动重装载(32bits)
#define TIM0_IE             0       // 1:开启中断,0:不开启中断
#define TIM0_RUN            0       // 1-初始化后立即运行，0-初始化后不运行

#define TIM1_PRS            0       // 0~7,预分频：1,2,4,8,16,32,64,256。
#define TIM1_CYCLE  	  	((PCLK*2)/(UART1_BAUD_RATE*32))       // 16bits 或者 32bits  设置定时器周期时钟数，波特率计算公式
#define TIM1_AUTO_RELOAD    1       // 1:自动重装载（16bits），0:不自动重装载(32bits)
#define TIM1_IE             0       // 1:开启中断,0:不开启中断
#define TIM1_RUN            0       // 1-初始化后立即运行，0-初始化后不运行

#define TIM2_PRS            7       // 0~7,预分频：1,2,4,8,16,32,64,256。
#define TIM2_CYCLE  	  	9375ul  // 16bits 或者 32bits 整数  设置定时器周期时钟数
#define TIM2_AUTO_RELOAD    1       // 1:自动重装载（16bits），0:不自动重装载(32bits)
#define TIM2_IE             1       // 1:开启中断,0:不开启中断
#define TIM2_RUN            0       // 1-初始化后立即运行，0-初始化后不运行

#endif

void HC32TIM0_Init();
void HC32TIM1_Init();
void HC32TIM2_Init();

STATIC_FORCE_IN_LINE void HC32TIM0_DisableInt()
{
    M0P_TIM0->CR_f.UIE = 0;
}

STATIC_FORCE_IN_LINE void HC32TIM0_EnableInt()
{
    M0P_TIM0->CR_f.UIE = 1;
}

STATIC_FORCE_IN_LINE void HC32TIM0_Run()
{
    M0P_TIM0->CR_f.CTEN = 1;
}

STATIC_FORCE_IN_LINE void HC32TIM0_Stop()
{
    M0P_TIM0->CR_f.CTEN = 0;
}


STATIC_FORCE_IN_LINE void HC32TIM1_DisableInt()
{
    M0P_TIM1->CR_f.UIE = 0;
}

STATIC_FORCE_IN_LINE void HC32TIM1_EnableInt()
{
    M0P_TIM1->CR_f.UIE = 1;
}

STATIC_FORCE_IN_LINE void HC32TIM1_Run()
{
    M0P_TIM1->CR_f.CTEN = 1;
}

STATIC_FORCE_IN_LINE void HC32TIM1_Stop()
{
    M0P_TIM1->CR_f.CTEN = 0;
}

STATIC_FORCE_IN_LINE void HC32TIM2_DisableInt()
{
    M0P_TIM2->CR_f.UIE = 0;
}

STATIC_FORCE_IN_LINE void HC32TIM2_EnableInt()
{
    M0P_TIM2->CR_f.UIE = 1;
}

STATIC_FORCE_IN_LINE void HC32TIM2_Run()
{
    M0P_TIM2->CR_f.CTEN = 1;
}

STATIC_FORCE_IN_LINE void HC32TIM2_Stop()
{
    M0P_TIM2->CR_f.CTEN = 0;
}

// 使用结构体配置参数的初始化：
// typedef enum _base_timer_prescale
// {
//     BASE_TIMER_PRE_DIV_1 = 0,
//     BASE_TIMER_PRE_DIV_2 = 1,
//     BASE_TIMER_PRE_DIV_4 = 2,
//     BASE_TIMER_PRE_DIV_8,
//     BASE_TIMER_PRE_DIV_16,
//     BASE_TIMER_PRE_DIV_32,
//     BASE_TIMER_PRE_DIV_64,
//     BASE_TIMER_PRE_DIV_256
// }base_timer_prescale_t;

// typedef struct _base_timer_cfg
// {
//     base_timer_prescale_t prescale;     // 预分频
//     u32 count_cycles;                   // 16bits 或者 32bits  设置定时器周期时钟数
//     u32 auto_reload;                   // 1 自动重装载（16bits），0 不自动重装载(32bits)
//     u32 enable_interrupt;              // 1,0
// }base_timer_cfg_t;

// void BaseTim_Init(base_timer_cfg_t cfg);

#endif

#ifdef __cplusplus
}
#endif


#endif


