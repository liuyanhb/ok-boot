/***********************************************************************************
 * 文件名： hc32_flash_ll.h
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		HC32 的 Flash 低层操作，提供整页擦写API。
 * 修改记录：
 * 	2020/8/20: 初版。 刘言。
***********************************************************************************/

#include "mcu.h"

// 参数定义

#define FLASH_CLK           F_PCLK      // Flash时钟频率 MHz

#ifndef HC32_EXIST_CONFIG_FILE

#define FLASH_PAGE_SIZE     512
#define FLASH_UNLOCK_VALUE  0XFFFFFFFF

#endif

// 自定义类型

typedef u32	ofaddr_t;			//片上FLASH地址类型，取决于地址需要占用的字节数
typedef u32 ofsize_t;             // 尺寸（长度）类型，取决于一次最大写入长度


// 提供的API

void FLASH_Lock();
void FLASH_Init();
void FLASH_WritePage(const u8 *buff, ofaddr_t addr);





