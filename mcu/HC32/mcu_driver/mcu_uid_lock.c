/***********************************************************************************
 * 文件名： mcu_uid_lock.c
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		利用UID加密。依赖 flash 驱动。
 * 修改记录：
 * 	2020/8/19: 初版。 刘言。
***********************************************************************************/

#include "HC32_LyLib.h"
#include "mcu_onchip_flash.h"
#include "mcu_uid_lock.h"

extern const uid_pram_t mUidPram;		//统一定义在外部文件便于地址管控


static u32 ColMatchCode();

void ULock_Init()
{
	if(mUidPram.UidMatchCode == UL_INIT_KEY)
	{
		u32 mcode = ColMatchCode();
        OcpFlash_Write((const u8 *)&mcode, (ofaddr_t)&mUidPram.UidMatchCode, 4);
	}
}

u8 *ULock_GetUid()
{
	return (u8 *)UID_ADDR;
}

int main();
void ULock_Verify()	//如果匹配码不正确，跳转到main（或者改为其他函数）。在程序任意位置调用（想要检查UID匹配的位置）
{
	if(mUidPram.UidMatchCode != ColMatchCode())
	{
		main();
	}
}

static u32 ColMatchCode()	//计算匹配码 UID->算法->匹配码
{
	u8 i,a;
	u8 *addr;
	u32 Code=0;
	
	addr=(u8 *)UID_ADDR;

	//读取UID并计算出 匹配码Code
	for(i=0;i<UID_LEN;i++)
	{
		a=*addr++;
		Code+=a*0XFEDC+i;
	}
	return Code;
}

