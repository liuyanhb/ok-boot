/***********************************************************************************
 * 文件名： hc32_general_timer.h
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		通用定时器驱动文件。注意HC32F005系列没有通用定时器，它们是基本定时器，见 hc32_base_timer.h。
 *      中断入口统一定义在 isr.c 中。
 * 修改记录：
 * 	2020/8/19: 初版。 刘言。
***********************************************************************************/
#ifndef _HC32_GENERAL_TIMER_H_
#define _HC32_GENERAL_TIMER_H_

#ifdef __cplusplus
 extern "C" {
#endif

#if (HC32_MCU_SERIES != HC32F005)



#ifndef HC32_EXIST_CONFIG_FILE

// 以下为模式0的配置，目前本驱动只支持模式0

#define TIM0_PRS            0       // 0~7,预分频：PCLK / 1,2,4,8,16,32,64,256。
#define TIM0_CYCLE  	  	1       // 16bits 或者 32bits 整数  设置定时器周期时钟数
#define TIM0_AUTO_RELOAD    0       // 1:自动重装载（16bits），0:不自动重装载(32bits)
#define TIM0_UIE            0       // 1:开启中断,0:不开启中断

#define TIM1_PRS            0       // 0~7,预分频：PCLK / 1,2,4,8,16,32,64,256。
#define TIM1_CYCLE  	  	1       // 16bits 或者 32bits 整数  设置定时器周期时钟数
#define TIM1_AUTO_RELOAD    1       // 1:自动重装载（16bits），0:不自动重装载(32bits)
#define TIM1_UIE            0       // 1:开启中断,0:不开启中断

#define TIM2_PRS            7       // 0~7,预分频：PCLK / 1,2,4,8,16,32,64,256。
#define TIM2_CYCLE  	  	9375ul  // 16bits 或者 32bits 整数  设置定时器周期时钟数
#define TIM2_AUTO_RELOAD    1       // 1:自动重装载（16bits），0:不自动重装载(32bits)
#define TIM2_UIE            1       // 1:开启中断,0:不开启中断

#endif

void HC32TIM0_Init();
void HC32TIM1_Init();
void HC32TIM2_Init();

#endif

#ifdef __cplusplus
}
#endif


#endif


