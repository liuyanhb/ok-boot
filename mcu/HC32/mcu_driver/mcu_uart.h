#ifndef _MCU_UART_H_
#define _MCU_UART_H_

#ifdef __cplusplus
 extern "C" {
#endif

#include "typedef.h"
#include "hc32_base_uart.h"
#include "hc32_general_uart.h"

// 提供的接口声明

#define Uart0_Init()                 UART0_Init()
#define Uart0_SendByte(c)            UART0_SendByte(c)
#define Uart0_Send(dat,len)          UART0_Send(dat,len)
#define Uart0_SetBaud(b)             UART0_SetBaud(b)
#define Uart0_GetRecivedFlag()       UART0_GetRecivedFlag()
#define Uart0_ClearRecivedFlag()     UART0_ClearRecivedFlag()
#define Uart0_GetRecivedbyte()       UART0_GetRecivedbyte()

#define Uart1_Init()                 UART1_Init()
#define Uart1_SendByte(c)            UART1_SendByte(c)
#define Uart1_Send(dat,len)          UART1_Send(dat,len)
#define Uart1_SetBaud(b)             UART1_SetBaud(b)
#define Uart1_GetRecivedFlag()       UART1_GetRecivedFlag()
#define Uart1_ClearRecivedFlag()     UART1_ClearRecivedFlag()
#define Uart1_GetRecivedbyte()       UART1_GetRecivedbyte()


#ifdef __cplusplus
}
#endif

#endif
