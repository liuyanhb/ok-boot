/***********************************************************************************
 * 文件名： mcu_timer.h
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		提供统一的硬件定时器API，中断入口统一在ISR.C中。
 *      注意，HC32F005系列中的 TIM0 1 2 是基本定时器，其它系列中是通用定时器，功能有所区别。
 * 修改记录：
 * 	2020/8/18: 初版。 刘言。
***********************************************************************************/

#ifndef _MCU_TIMER_H_
#define _MCU_TIMER_H_

#ifdef __cplusplus
 extern "C" {
#endif

#include "hc32_adv_timer.h"
#include "hc32_base_timer.h"
#include "hc32_general_timer.h"

// 引用声明

// 提供的接口声明

#define Tim0_Init()         HC32TIM0_Init()
#define Tim0_DisableInt()   HC32TIM0_DisableInt()
#define Tim0_EnableInt()    HC32TIM0_EnableInt()
#define Tim0_Run()          HC32TIM0_Run()
#define Tim0_Stop()         HC32TIM0_Stop()

#define Tim1_Init()         HC32TIM1_Init()
#define Tim1_DisableInt()   HC32TIM1_DisableInt()
#define Tim1_EnableInt()    HC32TIM1_EnableInt()
#define Tim1_Run()          HC32TIM1_Run()
#define Tim1_Stop()         HC32TIM1_Stop()

#define Tim2_Init()         HC32TIM2_Init()
#define Tim2_DisableInt()   HC32TIM2_DisableInt()
#define Tim2_EnableInt()    HC32TIM2_EnableInt()
#define Tim2_Run()          HC32TIM2_Run()
#define Tim2_Stop()         HC32TIM2_Stop()

#define Tim4_Init()     HC32TIM4_Init()
#define Tim4_Run()      HC32TIM4_Run()
#define Tim4_Stop()     HC32TIM4_Stop()
// 设置指定通道的比较值。一般用量更改PWM占空比。
// ch - 通道号，0~1 分别代表 A~B。
// value - 比较值。
#define Tim4_SetCompValue(ch, value)     HC32TIM4_SetCompValue(ch, value)

#define Tim5_Init()     HC32TIM5_Init()
#define Tim5_Run()      HC32TIM5_Run()
#define Tim5_Stop()     HC32TIM5_Stop()
// 设置指定通道的比较值。一般用量更改PWM占空比。
// ch - 通道号，0~1 分别代表 A~B。
// value - 比较值。
#define Tim5_SetCompValue(ch, value)     HC32TIM5_SetCompValue(ch, value)

#define Tim6_Init()     HC32TIM6_Init()
#define Tim6_Run()      HC32TIM6_Run()
#define Tim6_Stop()     HC32TIM6_Stop()
// 设置指定通道的比较值。一般用量更改PWM占空比。
// ch - 通道号，0~1 分别代表 A~B。
// value - 比较值。
#define Tim6_SetCompValue(ch, value)     HC32TIM6_SetCompValue(ch, value)


#ifdef __cplusplus
}
#endif

#endif



