/***********************************************************************************
 * 文件名： hc32_general_timer.c
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		通用定时器驱动文件。注意HC32F005系列没有通用定时器，它们是基本定时器，见 hc32_base_timer.c。
 *      中断入口统一定义在 isr.c 中。
 * 修改记录：
 * 	2020/8/19: 初版。 刘言。
***********************************************************************************/

#include "HC32_LyLib.h"
#include "HC32_general_timer.h"

#if (HC32_MCU_SERIES != HC32F005)

void HC32TIM0_Init()
{
    M0P_TIM0_MODE0->ICLR = 0;               // 清除状态标志位
#if (TIM0_AUTO_RELOAD == 1)
    M0P_TIM2_MODE0->ARR = 0x10000-TIM0_CYCLE;       // 重载值
    M0P_TIM2_MODE0->CNT = 0x10000-TIM0_CYCLE;       // 计数初值
#else
    M0P_TIM0_MODE0->CNT32 = 0X100000000UL - TIM0_CYCLE;  // 计数初值
#endif
    M0P_TIM0_MODE0->M0CR =   (TIM1_UIE<<10)          // 是否开启中断
                    |(TIM1_PRS<<4)          // 预分频 TIM1_PRS
                    |(TIM1_AUTO_RELOAD<<1)  // 自动重装载模式
                    |(1<<0);                // 定时器开始运行

#if (TIM0_IE == 1)
    NVIC_ClearPendingIRQ(TIM0_IRQn);        // 清除NVIC中断标志位
    NVIC_SetPriority(TIM0_IRQn, 3);         // 优先级 3 （0~3）
    NVIC_EnableIRQ(TIM0_IRQn);              // NVIC允许中断
#endif
}

void HC32TIM1_Init()
{
    M0P_TIM1_MODE0->ICLR = 0;               // 清除状态标志位
#if (TIM1_AUTO_RELOAD == 1)
    M0P_TIM1_MODE0->ARR = 0x10000-TIM1_CYCLE;       // 重载值
    M0P_TIM1_MODE0->CNT = 0x10000-TIM1_CYCLE;       // 计数初值
#else
    M0P_TIM2_MODE0->CNT32 = 0X100000000UL - TIM1_CYCLE;  // 计数初值
#endif
    M0P_TIM1_MODE0->M0CR =   (TIM1_UIE<<10)          // 是否开启中断
                    |(TIM1_PRS<<4)          // 预分频 TIM1_PRS
                    |(TIM1_AUTO_RELOAD<<1)  // 自动重装载模式
                    |(1<<0);                // 定时器开始运行

#if (TIM1_IE == 1)
    NVIC_ClearPendingIRQ(TIM1_IRQn);        // 清除NVIC中断标志位
    NVIC_SetPriority(TIM1_IRQn, 3);         // 优先级 3 （0~3）
    NVIC_EnableIRQ(TIM1_IRQn);              // NVIC允许中断
#endif
}

void HC32TIM2_Init()
{
    M0P_TIM2_MODE0->ICLR = 0;               // 清除状态标志位
#if (TIM2_AUTO_RELOAD == 1)
    M0P_TIM2_MODE0->ARR = 0x10000-TIM2_CYCLE;       // 重载值
    M0P_TIM2_MODE0->CNT = 0x10000-TIM2_CYCLE;       // 计数初值
#else
    M0P_TIM2_MODE0->CNT32 = 0X100000000UL - TIM2_CYCLE;  // 计数初值
#endif
    M0P_TIM2_MODE0->M0CR =   (TIM1_UIE<<10)          // 是否开启中断
                    |(TIM1_PRS<<4)          // 预分频 TIM1_PRS
                    |(TIM1_AUTO_RELOAD<<1)  // 自动重装载模式
                    |(1<<0);                // 定时器开始运行

#if (TIM2_IE == 1)
    NVIC_ClearPendingIRQ(TIM2_IRQn);        // 清除NVIC中断标志位
    NVIC_SetPriority(TIM2_IRQn, 3);         // 优先级 3 （0~3）
    NVIC_EnableIRQ(TIM2_IRQn);              // NVIC允许中断
#endif
}


#endif




