/***********************************************************************************
 * 文件名： mcu_power.c
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		MCU的电源管理驱动。
 * 修改记录：
 * 	2020/8/19: 初版。 刘言。
***********************************************************************************/
#include "HC32_LyLib.h"
#include "mcu_power.h"
#include "typedef.h"


// 进入深度休眠模式，自行确认在此之前设置了支持的唤醒中断
void Pow_Sleep()
{
    // IO设置

    SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;      // 即将进入深度休眠
    //SCB->SCR &= ~SCB_SCR_SLEEPONEXIT_Msk;
    __WFI();    // 休眠

    // IO恢复
}

// 重启（复位）。注意所有寄存器将回到复位初始值。
IN_LINE void Pow_Reset()
{
    NVIC_SystemReset();
}
