/***********************************************************************************
 * 文件名： mcu_adc.c
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		ADC驱动。
 * 修改记录：
 * 	2020/8/19: 初版。 刘言。
***********************************************************************************/

#ifndef _MCU_ADC_H_
#define _MCU_ADC_H_

#include "mcu.h"

// 可选的参考电压源
typedef enum _adc_vref_t
{
    ADC_VREF_ON_CHIP_1500,
    ADC_VREF_ON_CHIP_2500,
    ADC_VREF_EXTERNAL,
    ADC_VREF_VDD
}adc_vref_t;


#ifndef HC32_EXIST_CONFIG_FILE

// ADC 初始参数配置

#define ADC_CLKSEL      3   // 0~3 ADC时钟分频 = 2^ADC_CLKSEL , FADC = PCLK/2^ADC_CLKSEL = 24/8=3MHz
#define ADC_SAM         3   // 0~3 ADC采样周期：4,6,8,12，ADC转换周期 = 采样周期 + 16 = 28 个时钟周期,107kHz 9.333us 
#define ADC_SREF        3   // 0~3 ADC参考电压：内部1.5V，内部2.5V，外部P36，电源电压
#define ADC_BUFEN       1   // 0 关闭，1 开启 信号放大器（电压跟随器）


#if (HC32_MCU_SERIES == HC32F005)

#define ADC_SCAN_CNT            3       // 0~7 ADC扫描模式转换次数，大于等于扫描模式开启的通道数，=0不使用扫描功能（可节省代码量）
#define ADC_SCAN_CHEN_MASK      0xA1    // 1010 0001 。扫描模式开启的通道，1表示开启。
#define ADC_TIMING_SCAN         1       // 1：启用定时扫描功能，依据所使用的定时器进行下列项目的配置
    #define ADC_DISALLOW_INT_START_SCAN     Tim1_DisableInt()   // 杜绝在中断中启动扫描转换的可能性
    #define ADC_ALLOW_INT_START_SCAN        Tim1_EnableInt()    // 允许在中断中启动扫描转换
    #define ADC_START_TIMING_SCAN           Tim1_Run()          // 开始定时扫描
    #define ADC_STOP_TIMING_SCAN            Tim1_Stop()         // 停止定时扫描

#endif

#endif



// 参数计算
#if (ADC_SREF == 0)
#define ADC_FULL_VOLTAGE    1500
#elif (ADC_SREF == 1)
#define ADC_FULL_VOLTAGE    2500
#else
//#define ADC_FULL_VOLTAGE    5000  // 自己定义
#endif



//APIs

#define ADC_FULL_VALUE 4095

#if (ADC_SCAN_CNT > 0)
// 开始定时扫描，定时间隔时间取决于所使用的定时器配置
// 注意：所使用的定时器需要自行初始化配置
#define Adc_StartTimingScan()   ADC_START_TIMING_SCAN
// 停止定时扫描
#define Adc_StopTimingScan()    ADC_STOP_TIMING_SCAN
#endif

void Adc_Init();
#if (ADC_SCAN_CNT > 0)
void Adc_ScanCompleted();
void Adc_StartScan();
#endif
u16 Adc_GetOnce(u8 ch);
u16 Adc_GetValue(u8 ch);
u16 Adc_GetVDDVoltage();

// void Adc_SetVref(adc_vref_t vref);

#endif


