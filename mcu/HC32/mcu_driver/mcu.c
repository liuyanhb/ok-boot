/***********************************************************************************
 * 文件名： mcu.c
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		MCU 初始化
 * 修改记录：
 * 	2020/8/18: 初版。 刘言。
***********************************************************************************/
#include "mcu.h"

#ifndef __VTOR_PRESENT
#define SCB_VTOR (*(vu32 *)(0xE000ED08)) // 从厂家提供的汇编启动文件可知 VTOR 是存在的。
#else
#define SCB_VTOR SCB->VTOR
#endif

void SystemInit(void) // 汇编启动代码会先调用这个，在调用main之前
{
  __disable_irq();		// 关全局中断

	// 中断向量表地址初始化
#if (FLASH_BASE != 0)
	SCB_VTOR = FLASH_BASE; /* Vector Table Relocation in Internal FLASH. */
	// 确保接下来的所有指令都使用新配置
	__DSB();
	__ISB();
#endif

#if (CLOCK_SOURCE == 1) //使用外部晶振
#error "目前为止还不支持设置外部晶振！"
#else
  ///<============== 将时钟从无法确定的默认切换至RCH ==============================
  // HC32 默认的时钟不是准确的4MHZ。所以即便是4MHZ也要切换。
  _SYSCTRL_UNLOCK;
  M0P_SYSCTRL->SYSCTRL0 = 0X000001C1; //M0P_SYSCTRL->SYSCTRL0_f.HCLK_PRS = 7;
  SYSCTRL_SET_RCH_4MHZ;
#if (F_HSI == 8)
  SYSCTRL_SET_RCH_8MHZ;
#elif (F_HSI == 16)
  SYSCTRL_SET_RCH_8MHZ;
  SYSCTRL_SET_RCH_16MHZ;
#elif (F_HSI == 24)
  SYSCTRL_SET_RCH_8MHZ;
  SYSCTRL_SET_RCH_16MHZ;
  SYSCTRL_SET_RCH_24MHZ;
#endif
  _SYSCTRL_UNLOCK;
  M0P_SYSCTRL->SYSCTRL0 = (MCU_PCLK_PRS << 9)   // PCLK分频
                          |(MCU_HCLK_PRS << 6)  // HCLK分频
                          |(1 << 0);
#endif

  // 开启外设时钟
  M0P_SYSCTRL->PERI_CLKEN = PERI_CLKEN_INIT;
}

// 系统反初始化
// BootLoader程序可能需要将MCU寄存器还原到复位状态
IN_LINE void SystemDeInit(void)
{
  M0P_SYSCTRL->PERI_CLKEN = 0XC0800000; // 复位默认值
}




