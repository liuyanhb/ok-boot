/***********************************************************************************
 * 文件名： mcu_onchip_flash.h
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		片上FLASH驱动，提供写入API。
 * 修改记录：
 * 	2020/8/19: 初版。 刘言。
***********************************************************************************/
#ifndef _MCU_ONCHIP_FLASH_H_
#define _MCU_ONCHIP_FLASH_H_

#include "hc32_flash_ll.h"


// 参数

#ifndef HC32_EXIST_CONFIG_FILE

#define FLASH_SIZE          (15 * 1024)

#endif


// APIs

// 片上FLASH初始化
#define  OcpFlash_Init()    FLASH_Init()

// 写入
bool OcpFlash_Write(const u8 *dat, ofaddr_t addr, ofsize_t length);

#endif


