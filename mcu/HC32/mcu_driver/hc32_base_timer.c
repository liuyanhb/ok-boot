/***********************************************************************************
 * 文件名： hc32_base_timer.c
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		仅 HC32F005 系列拥有 base timer，它们是 TIM0 TIM1 TIM2，其它型号MCU请使用 
 *  hc32_general_timer.c 。
 *      中断入口统一定义在 isr.c 中。
 * 修改记录：
 * 	2020/8/18: 初版。 刘言。
***********************************************************************************/

#include "mcu.h"
#include "hc32_base_timer.h"

#if (HC32_MCU_SERIES == HC32F005)

void HC32TIM0_Init()
{
    M0P_TIM0->ICLR = 0;               // 清除状态标志位
#if (TIM0_AUTO_RELOAD == 1)
    M0P_TIM0->ARR = 0x10000-TIM0_CYCLE;       // 重载值
    M0P_TIM0->CNT = 0x10000-TIM0_CYCLE;       // 计数初值
#else
    M0P_TIM0->CNT32 = 0X100000000UL - TIM0_CYCLE;  // 计数初值
#endif
    M0P_TIM0->CR =   (TIM0_IE<<10)          // 是否开启中断
                    |(TIM0_PRS<<4)          // 预分频 TIM1_PRS
                    |(TIM0_AUTO_RELOAD<<1)  // 自动重装载模式
                    |(TIM0_RUN<<0);                // 定时器开始运行

#if (TIM0_IE == 1)
    NVIC_ClearPendingIRQ(TIM0_IRQn);        // 清除NVIC中断标志位
    NVIC_SetPriority(TIM0_IRQn, 3);         // 优先级 3 （0~3）
    NVIC_EnableIRQ(TIM0_IRQn);              // NVIC允许中断
#endif
}

void HC32TIM1_Init()
{
    M0P_TIM1->ICLR = 0;               // 清除状态标志位
#if (TIM1_AUTO_RELOAD == 1)
    M0P_TIM1->ARR = 0x10000-TIM1_CYCLE;       // 重载值
    M0P_TIM1->CNT = 0x10000-TIM1_CYCLE;       // 计数初值
#else
    M0P_TIM1->CNT32 = 0X100000000UL - TIM1_CYCLE;  // 计数初值
#endif
    M0P_TIM1->CR =   (TIM1_IE<<10)          // 是否开启中断
                    |(TIM1_PRS<<4)          // 预分频 TIM1_PRS
                    |(TIM1_AUTO_RELOAD<<1)  // 自动重装载模式
                    |(TIM1_RUN<<0);                // 定时器开始运行

#if (TIM1_IE == 1)
    NVIC_ClearPendingIRQ(TIM1_IRQn);        // 清除NVIC中断标志位
    NVIC_SetPriority(TIM1_IRQn, 3);         // 优先级 3 （0~3）
    NVIC_EnableIRQ(TIM1_IRQn);              // NVIC允许中断
#endif
}

void HC32TIM2_Init()
{
    M0P_TIM2->ICLR = 0;               // 清除状态标志位
#if (TIM2_AUTO_RELOAD == 1)
    M0P_TIM2->ARR = 0x10000-TIM2_CYCLE;       // 重载值
    M0P_TIM2->CNT = 0x10000-TIM2_CYCLE;       // 计数初值
#else
    M0P_TIM2->CNT32 = 0X100000000UL - TIM2_CYCLE;  // 计数初值
#endif
    M0P_TIM2->CR =   (TIM2_IE<<10)          // 是否开启中断
                    |(TIM2_PRS<<4)          // 预分频 TIM1_PRS
                    |(TIM2_AUTO_RELOAD<<1)  // 自动重装载模式
                    |(TIM2_RUN<<0);                // 定时器开始运行

#if (TIM2_IE == 1)
    NVIC_ClearPendingIRQ(TIM2_IRQn);        // 清除NVIC中断标志位
    NVIC_SetPriority(TIM2_IRQn, 3);         // 优先级 3 （0~3）
    NVIC_EnableIRQ(TIM2_IRQn);              // NVIC允许中断
#endif
}

#endif



