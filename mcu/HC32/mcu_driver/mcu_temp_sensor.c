/***********************************************************************************
 * 文件名： mcu_temp_sensor.c
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		MCU内置的温度传感器驱动，提供获取温度值的API。
 *      依赖于 mcu_adc 
 * 修改记录：
 * 	2020/12/21: 初版。 刘言。
***********************************************************************************/
#include "mcu_adc.h"
#include "mcu_temp_sensor.h"

#if (ADC_BUFEN != 1)
#error "要使用温度传感器，ADC BUFEN 必须开启。"
#endif

#if (HC32_MCU_SERIES == HC32F005)
    #define _MCU_TEMP_SENSOR_ADCH 10
#else
    #define _MCU_TEMP_SENSOR_ADCH 28
#endif

#define _MCU_TEMP_SENSOR_TRIM_15 (*((vu16 *)0X00100C34ul))

// 获取温度值（℃），调用之前确保ADC的基准电压设置在1.5V档，否则温度值不准确。
int OcpSensor_GetTemp()
{
    int temp;
    int temp_value = Adc_GetValue(_MCU_TEMP_SENSOR_ADCH);
    temp = 25 + (int)839 * 15 * (temp_value - _MCU_TEMP_SENSOR_TRIM_15) / 100000;
    return temp;
}
