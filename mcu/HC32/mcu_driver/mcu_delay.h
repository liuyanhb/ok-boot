/***********************************************************************************
 * 文件名： mcu_delay.h
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		利用定时器精确延时。ARM内核一般使用SysTick定时器，不影响周期定时中断。
 * 修改记录：
 * 	2020/8/19: 初版。 刘言。
***********************************************************************************/
#ifndef _MCU_DELAY_H_
#define _MCU_DELAY_H_

#include "typedef.h"

#ifndef HC32_EXIST_CONFIG_FILE

#define DELAY_TIM_INIT                          // Delay 所使用的定时器初始化，这里使用SysTick，已经被初始化了。
#define DELAY_COUNTER           SysTick->VAL    // 计时寄存器，默认是递减计数
#define DELAY_COUNT_PERIOD      SYSTICK_VALUE   // 计数周期（重载值）
#define DELAY_COUNT_PER_US      24              // 1us 的计数个数 （计数的频率，单位MHz. SysTick会被库函数设置为HCLK）

#endif

void Delay_Init();
void Delay_us(u16 us);
void Delay_ms(u16 ms);

#endif