/***********************************************************************************
 * 文件名： mcu_pca.c
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		PCA(可编程计数阵列，可以认为是一个定时器)驱动。
 * 修改记录：
 * 	2020/12/25: 初版。 刘言。
***********************************************************************************/

#include "mcu_pca.h"

void Pca_Init()
{
    M0P_PCA->CMOD = (_PCA_CFIE << 0)
                    |(_PCA_CPS << 1)
                    |(_PCA_CIDL << 7)
                    ;
    M0P_PCA->CCAPM0 = (_PCA_CH0_CCIE << 0)
                    |(_PCA_CH0_PWM << 1)
                    |(_PCA_CH0_TOG << 2)
                    |(_PCA_CH0_MAT << 3)
                    |(_PCA_CH0_CAPN << 4)
                    |(_PCA_CH0_CAPP << 5)
                    |(_PCA_CH0_ECOM << 6)
                    ;
    M0P_PCA->CCAPM1 = (_PCA_CH1_CCIE << 0)
                    |(_PCA_CH1_PWM << 1)
                    |(_PCA_CH1_TOG << 2)
                    |(_PCA_CH1_MAT << 3)
                    |(_PCA_CH1_CAPN << 4)
                    |(_PCA_CH1_CAPP << 5)
                    |(_PCA_CH1_ECOM << 6)
                    ;
    M0P_PCA->CCAPM2 = (_PCA_CH2_CCIE << 0)
                    |(_PCA_CH2_PWM << 1)
                    |(_PCA_CH2_TOG << 2)
                    |(_PCA_CH2_MAT << 3)
                    |(_PCA_CH2_CAPN << 4)
                    |(_PCA_CH2_CAPP << 5)
                    |(_PCA_CH2_ECOM << 6)
                    ;
    M0P_PCA->CCAPM3 = (_PCA_CH3_CCIE << 0)
                    |(_PCA_CH3_PWM << 1)
                    |(_PCA_CH3_TOG << 2)
                    |(_PCA_CH3_MAT << 3)
                    |(_PCA_CH3_CAPN << 4)
                    |(_PCA_CH3_CAPP << 5)
                    |(_PCA_CH3_ECOM << 6)
                    ;
    M0P_PCA->CCAPM4 = (_PCA_CH4_CCIE << 0)
                    |(_PCA_CH4_PWM << 1)
                    |(_PCA_CH4_TOG << 2)
                    |(_PCA_CH4_MAT << 3)
                    |(_PCA_CH4_CAPN << 4)
                    |(_PCA_CH4_CAPP << 5)
                    |(_PCA_CH4_ECOM << 6)
                    ;
    M0P_PCA->CCAP0 = 0;
    M0P_PCA->CCAP1 = 0;
    M0P_PCA->CCAP2 = 0;
    M0P_PCA->CCAP3 = 0;
    M0P_PCA->CCAP4 = 0;

    
    M0P_PCA->CCON = (_PCA_RUN<<6);     // 启动计数
}


