/***********************************************************************************
 * 文件名： hc32_general_uart.c
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		通用 UART 驱动。注意，HC32F005系列的UART是基本UART，见 hc32_base_uart.c。
 *      中断入口统一定义在 isr.c 中。
 * 修改记录：
 * 	2020/8/19: 初版。 刘言。
***********************************************************************************/
#include "HC32_LyLib.h"
#include "hc32_general_uart.h"		

#if (HC32_MCU_SERIES != HC32F005)

void UART0_Init()
{
    // 端口设置
    // GPIO_DIR_OUTPUT(GPIO_PORT_A, 9);
    // GPIO_PA09_SEL_UART0_TXD;
    // //GPIO_DIR_INPUT(GPIO_PORT_A, 10); 默认
    // GPIO_PA10_SEL_UART0_RXD;

    // 时钟门控
    // SYSCTRL_ENABLE_UART1_CLK; 已统一开启(mcu.c中)

    // UART0 具体设置
    M0P_UART0->SCON =   (0 << 14)   // 1停止位
                      | (1 << 9)    // 双倍波特率（OVER = 8）
                      | (1 << 6)    // 模式1： 异步模式全双工，1起始位，8数据位；
                      | (1 << 4)    // 使能收发
                      | (0 << 1)    // 关闭发送中断
                      | (UART0_RIEN << 0) // 设置接收中断
        ;
    M0P_UART0->ICR = 0; // 清除标志位
    M0P_UART0->SCNT = UART_PCLK / ( 8 * UART0_BAUD_RATE);    // 设置波特率

#if (UART0_RIEN == 1)
    NVIC_ClearPendingIRQ(UART0_IRQn); // 清除NVIC中断标志位
    NVIC_SetPriority(UART0_IRQn, 3);  // 优先级 3 （0~3）
    NVIC_EnableIRQ(UART0_IRQn);       // NVIC允许中断
#endif
}

void UART1_Init()
{
    // 端口设置
    // GPIO_DIR_OUTPUT(GPIO_PORT_D, 0);
    // GPIO_PD00_SEL_UART1_TXD;
    // //GPIO_DIR_INPUT(GPIO_PORT_D, 1); 默认
    // GPIO_PD01_SEL_UART1_RXD;

    // 时钟门控
    // SYSCTRL_ENABLE_UART1_CLK; 已统一开启(mcu.h中)

    // UART1 具体设置
    M0P_UART1->SCON =   (0 << 14)   // 1停止位
                      | (1 << 9)    // 双倍波特率（OVER = 8）
                      | (1 << 6)    // 模式1： 异步模式全双工，1起始位，8数据位；
                      | (1 << 4)    // 使能收发
                      | (0 << 1)    // 关闭发送中断
                      | (UART1_RIEN << 0) // 设置接收中断
        ;
    M0P_UART1->ICR = 0; // 清除标志位
    M0P_UART1->SCNT = UART_PCLK / ( 8 * UART1_BAUD_RATE);    // 设置波特率

#if (UART1_RIEN == 1)
    NVIC_ClearPendingIRQ(UART1_IRQn); // 清除NVIC中断标志位
    NVIC_SetPriority(UART1_IRQn, 3);  // 优先级 3 （0~3）
    NVIC_EnableIRQ(UART1_IRQn);       // NVIC允许中断
#endif
}

// 查询模式发送一个字节
void UART0_SendByte(u8 c)
{
    M0P_UART0->SBUF = c;
    while(M0P_UART0->ISR_f.TC == 0);
    M0P_UART0->ICR_f.TCCF = 0;
}

// 查询模式发送一个字节
void UART1_SendByte(u8 c)
{
    M0P_UART1->SBUF = c;
    while(M0P_UART1->ISR_f.TC == 0);
    M0P_UART1->ICR_f.TCCF = 0;
}

// 发送一串数据
void UART0_Send(const u8 *buff, u16 Length)
{
    while(Length--)
	{
		UART0_SendByte(*buff);
		buff++;
	}
}

// 发送一串数据
void UART1_Send(const u8 *buff, u16 Length)
{
    while(Length--)
	{
		UART1_SendByte(*buff);
		buff++;
	}
}

// 设置波特率
void UART0_SetBaud(u32 baud)
{
    M0P_UART0->SCNT = UART_PCLK / ( 8 * baud);    // 设置波特率
}

// 设置波特率
void UART1_SetBaud(u32 baud)
{
    M0P_UART1->SCNT = UART_PCLK / ( 8 * baud);    // 设置波特率
}


#endif

