/***********************************************************************************
 * 文件名： mcu_delay.c
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		利用定时器精确延时。
 * 修改记录：
 * 	2020/8/19: 初版。 刘言。
***********************************************************************************/
#include "mcu.h"	
#include "mcu_delay.h"



IN_LINE void Delay_Init()
{
    DELAY_TIM_INIT;
}

void Delay_us(u16 us)
{
    u32 oldcnt, newcnt; 
    u32 tcntvalue = 0;	/* 走过的总时间(时钟数)  */

    oldcnt = DELAY_COUNTER; // DELAY_COUNTER 递减计数，计数周期时钟数为 DELAY_COUNT_PERIOD
    while(tcntvalue < us * DELAY_COUNT_PER_US)
    {
        newcnt = DELAY_COUNTER;
        if(newcnt != oldcnt)
        {
            if(oldcnt > newcnt) // 说明递减计数没有重载
                tcntvalue += oldcnt - newcnt;
            else                // 发生重载
                tcntvalue += oldcnt + (DELAY_COUNT_PERIOD - newcnt);
            oldcnt = newcnt;
        }
    }
}

void Delay_ms(u16 ms)
{
    while(ms--)
	{
		Delay_us(1000);
	}
}

