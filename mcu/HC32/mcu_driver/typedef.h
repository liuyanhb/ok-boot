/*
	以下定义适用于 ARM Cortex-M 
*/

#ifndef _TYPEDEF_H_
#define _TYPEDEF_H_

#include <stdint.h>	// ARMCLANG 编译器库文件
#include <stdbool.h>
#include <stddef.h>

typedef unsigned char  			byte;
typedef unsigned char  			u8;
typedef uint16_t  			    u16;
typedef uint32_t  			    u32;

typedef signed char  			s8;
typedef int16_t  			    s16;
typedef int32_t  			    s32;

typedef volatile unsigned char  vu8;
typedef volatile uint16_t 	    vu16;
typedef volatile uint32_t 	    vu32;

typedef volatile signed char  	vs8;
typedef volatile int16_t  		vs16;
typedef volatile int32_t  	    vs32;


//非51内核定义以下以关键字兼容51代码

    #define data
    #define idata 
    #define pdata 
    #define xdata     
    #define code
    #define bit   bool



//非STM8_IAR单片机定义以下关键字兼容STM8代码

    #define     __far
    #define     __near 
    #define     __no_init
    #define     __eeprom


#define IN_LINE     			__inline
#define FORCE_IN_LINE			__attribute__((always_inline)) __inline
#define STATIC_IN_LINE			static __inline
#define STATIC_FORCE_IN_LINE   	__attribute__((always_inline)) static __inline
/*
	这里有一个疑问 inline 、 __inline( = __INLINE) 都能编译通过, __forceinline 、 __attribute__((always_inline)) 也都能编译通过
	
	*/

typedef struct _tm
{
	u8 Second;
	u8 Minute;
	u8 Hour;
	u8 Day;
	u8 Week;
	u8 Month;
	u8 Year;
	u8 Zone;
}tm_t;


#endif
