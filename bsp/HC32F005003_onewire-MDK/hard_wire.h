
#include "mcu.h"

// 所有GPIO的初始状态
#define GPIO_INIT       \
{\
    /* 所有端口方向 1输入（默认）*/\
	/*M0P_GPIO->P0DIR = 0XFFFFFFFF;*/\
	/*M0P_GPIO->P1DIR = 0XFFFFFFFF;*/\
	M0P_GPIO->P2DIR = 0XFFFFFFF7;           /* 1111 0111 */\
	/*M0P_GPIO->P3DIR = 0XFFFFFFFF;*/\
    /* 所有端口输出值 */\
    M0P_GPIO->P2OUT = 0XFFFFFFF7;           /* 1111 0111 */\
	/* 所有端口模式 1模拟端口*/\
	/*M0P_GPIO->P0ADS = 0;*/\
	/*M0P_GPIO->P1ADS = 0;*/\
	/*M0P_GPIO->P2ADS = 0;*/\
	/*M0P_GPIO->P3ADS = 0;*/\
	/* 上拉配置 1开启 默认关闭*/\
	M0P_GPIO->P0PU = 0X0000FFF9;			/* 1111 1111 1111 1001*/\
	M0P_GPIO->P1PU = 0X0000FFFF;			/* 1111 1111 1111 1111*/\
	M0P_GPIO->P2PU = 0X0000FFF7;			/* 1111 1111 1111 0111*/\
	M0P_GPIO->P3PU = 0X0000FFFB;			/* 1111 1111 1111 1011*/\
	/* 下拉配置 1开启 默认关闭*/\
	/*M0P_GPIO->P0PD = 0X00000000;*/			/* 0000 0000 0000 0000*/\
	/*M0P_GPIO->P1PD = 0X00000000;*/			/* 0000 0000 0000 0000*/\
	/*M0P_GPIO->P2PD = 0X00000000;*/			/* 0000 0000 0000 0000*/\
	/*M0P_GPIO->P3PD = 0X00000000;*/			/* 0000 0000 0000 0000*/\
}

// 单线端口
#define WIRE_PIN    M0P_GPIO->P0IN_f.P01

// UART驱动已经完成了切换，所以以下定义为空
#define WIRE_PIN_UART_MODE		
// UART驱动已经完成了切换，所以以下定义为空	
#define WIRE_PIN_GPIO_INPUT_MODE	


// 使能外部强下拉
#define ENABLE_PULL_DOWN            M0P_GPIO->P2OUT = 0XFFFFFFFF
// 失能外部强下拉
#define DISABLE_PULL_DOWN           M0P_GPIO->P2OUT = 0XFFFFFFF7


