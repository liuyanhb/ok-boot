
#include "mcu.h"


#define UART1_PIN_INIT                      \
{                                           \
    M0P_GPIO->P3DIR = 0XFFFFFFFB;/*GPIO_DIR_OUTPUT(3, 2);*/                  \
    GPIO_P32_SEL_UART1_TXD;                 \
    /*GPIO_DIR_INPUT(3, 3);默认是输入*/         \
    GPIO_P33_SEL_UART1_RXD;                 \
}

