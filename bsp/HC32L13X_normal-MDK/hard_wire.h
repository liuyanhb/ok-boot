
#include "mcu.h"


#define UART0_PIN_INIT                      \
{                                           \
    GPIO_DIR_OUTPUT(GPIO_PORT_A, 9);\
    GPIO_PA09_SEL_UART0_TXD;\
    GPIO_PA10_SEL_UART0_RXD;\
}

#define UART1_PIN_INIT                      \
{                                           \
    GPIO_DIR_OUTPUT(GPIO_PORT_D, 0);\
    GPIO_PD00_SEL_UART1_TXD;\
    GPIO_PD01_SEL_UART1_RXD;\
}

