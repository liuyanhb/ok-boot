#ifndef _POLSYS_MISC_H_
#define _POLSYS_MISC_H_

#include "typedef.h"

#define __CONNECT(A,B)   A##B
#define _CONNECT(A,B)   __CONNECT(A,B)

//16 --> 8 x 2
#undef HIBYTE
#define HIBYTE(v1)              ((u8)((v1)>>8))                      //v1 is UINT16
#undef LOBYTE
#define LOBYTE(v1)              ((u8)((v1)&0xFF))
//8 x 2 --> 16
#undef MAKEWORD
#define MAKEWORD(v1,v2)         ((((u16)(v1))<<8)+(u16)(v2))      //v1,v2 is UINT8
#define MAKE_U16(h,l)           ((((u16)(h))<<8)+(u16)(l))      
#define MAKE_U32(hh,h,l,ll)     ((((u32)(hh))<<24)+(((u32)(h))<<16)+(((u32)(l))<<8) +(u32)(ll))
//32 --> 16 x 2
#undef YBYTE1
#define YBYTE1(v1)              ((u16)((v1)>>16))                    //v1 is UINT32
#undef YBYTE0
#define YBYTE0(v1)              ((u16)((v1)&0xFFFF))
//32 --> 8 x 4
#undef TBYTE3
#define TBYTE3(v1)              ((u8)((v1)>>24))                     //v1 is UINT32
#undef TBYTE2
#define TBYTE2(v1)              ((u8)((v1)>>16))
#undef TBYTE1
#define TBYTE1(v1)              ((u8)((v1)>>8)) 
#undef TBYTE0
#define TBYTE0(v1)              ((u8)((v1)&0xFF))

bool SumU32_Verify(const u8 *buff, u32 len);
u32 GetSum_U32(const u8 *buff, u32 len);
u8 GetSum_U8(const u8 *buff, u8 len);
u8 GetRand8bit(void);
bool IsInRangeU16(u16 num, u16 start, u16 end);
u32 MakeU32(u8 * Bytes,u8 Lenght);
u8 dec_to_bcd(u8 dat);
u8 bcd_to_dec(u8 dat);
u16 MedianFilter16b9(u16 *dat);
void ToString(char * str,float dat,const char * fmt);

#endif
