/***********************************************************************************
 * 文件名： BootLoader.c
 * 作者： 刘言
 * 版本： 20200827
 * 说明：
 * 		OneKB BootLoader 的实现。
 * 		本文件提供了以下接口函数：
 *      BootLoader_Start: 开始运行 BootLoader .
 * 修改记录：
 * 	2020/6/25: V0.1 初版。 刘言。
 *  2020/7/16: V0.2 解决了当升级时传输比较小的错误的APP数据时，这个错误的APP数据长度不足以
 *  覆盖APP存在标记，会误判断APP存在并跳转到APP导致系统崩溃问题。刘言。
 *  2020/8/3: V0.3 简化了函数调用。刘言。
 *  20200827: 增加了每次验证、CRC32校验、AES加密、多端口支持等功能。刘言。
***********************************************************************************/
#include "mcu.h" 
#include "encrypt_hjm.h"
#include "polsys_misc.h"
#include "BootLoader.h"

// 常量定义

#define UPDATA_NO           0
#define UPDATA_OK           1
#define UPDATA_TIMEOUT      2
#define UPDATA_CHKERR       3
#define UPDATA_SIZEERR      4

#if (USE_AES == 1)
static const u8 AES_KEY_ARRY[16] = AES_KEY;
#endif

#define APP_LEN_ADDR            (APP_FLAG_ADDR + 4)       // 这个值请保持默认，不要修改

// 静态变量定义

static u8 mBuff[FLASH_PAGE_SIZE + 128];     // 收发缓存
static u16 mPkgLen;                         // 数据包长度

#if (PORT_NUM > 1)
static int mReciveDataPortNum;          // 接收到了数据的端口号
static bool mPortLocked;                 // 端口已锁定标志
#endif

// 外部函数申明

// 跳转到APP（定义在GotoApp.s）
// app_addr: app所在地址 (ARM 规定第一个字（32bit）是SP初值，第二个字是ResetHander（启动入口）地址)。
// 初始化SP后直接跳转到启动入口，本函数不会返回。
void GotoApp(u32 app_addr);

// 静态函数申明

static void SendPkg(u16 lenght,u8 *dat);	//发送协议数据包
static bool ReciveByte(u8 *byte);
static bool RecivePkg();
static u8 Updata();
#ifdef USE_LOG
void Print(const char *s);
#endif

// 通信端口 API 定义

#if (PORT_NUM > 1)

#define Port0_Init                   _CONNECT(Port0,_Init)
#define Port0_SendByte               _CONNECT(Port0,_SendByte)
#define Port0_Send                   _CONNECT(Port0,_Send)
#define Port0_SetBaud                _CONNECT(Port0,_SetBaud)
#define Port0_GetRecivedFlag         _CONNECT(Port0,_GetRecivedFlag)
#define Port0_ClearRecivedFlag       _CONNECT(Port0,_ClearRecivedFlag)
#define Port0_GetRecivedbyte         _CONNECT(Port0,_GetRecivedbyte)

#define Port1_Init                   _CONNECT(Port1,_Init)
#define Port1_SendByte               _CONNECT(Port1,_SendByte)
#define Port1_Send                   _CONNECT(Port1,_Send)
#define Port1_SetBaud                _CONNECT(Port1,_SetBaud)
#define Port1_GetRecivedFlag         _CONNECT(Port1,_GetRecivedFlag)
#define Port1_ClearRecivedFlag       _CONNECT(Port1,_ClearRecivedFlag)
#define Port1_GetRecivedbyte         _CONNECT(Port1,_GetRecivedbyte)

#define Port2_Init                   _CONNECT(Port2,_Init)
#define Port2_SendByte               _CONNECT(Port2,_SendByte)
#define Port2_Send                   _CONNECT(Port2,_Send)
#define Port2_SetBaud                _CONNECT(Port2,_SetBaud)
#define Port2_GetRecivedFlag         _CONNECT(Port2,_GetRecivedFlag)
#define Port2_ClearRecivedFlag       _CONNECT(Port2,_ClearRecivedFlag)
#define Port2_GetRecivedbyte         _CONNECT(Port2,_GetRecivedbyte)

#define Port3_Init                   _CONNECT(Port3,_Init)
#define Port3_SendByte               _CONNECT(Port3,_SendByte)
#define Port3_Send                   _CONNECT(Port3,_Send)
#define Port3_SetBaud                _CONNECT(Port3,_SetBaud)
#define Port3_GetRecivedFlag         _CONNECT(Port3,_GetRecivedFlag)
#define Port3_ClearRecivedFlag       _CONNECT(Port3,_ClearRecivedFlag)
#define Port3_GetRecivedbyte         _CONNECT(Port3,_GetRecivedbyte)

#if (PORT_NUM == 2)
    #define Port_Init()\
    {\
        Port0_Init();\
        Port1_Init();\
    }
    #define Port_SendByte(a)\
    {\
        Port0_SendByte(a);\
        Port1_SendByte(a);\
    }
    #define Port_Send(a,b)\
    {\
        Port0_Send(a,b);\
        Port1_Send(a,b);\
    }
    #define Port_SetBaud(a)\
    {\
        Port0_SetBaud(a);\
        Port1_SetBaud(a);\
    }
    // 收到数据返回1，否则返回0，用mReciveDataPortNum保存收到数据的端口号。
    STATIC_IN_LINE int Port_GetRecivedFlag()
    {
        if(mPortLocked) // 端口已经锁定，只监视锁定的端口
        {
            if(mReciveDataPortNum == 0) return Port0_GetRecivedFlag();
            if(mReciveDataPortNum == 1) return Port1_GetRecivedFlag();
        }
        else
        {
            if(Port0_GetRecivedFlag()){mReciveDataPortNum = 0; return 1;}
            if(Port1_GetRecivedFlag()){mReciveDataPortNum = 1; return 1;}
        }
        return 0;
    }
    #define Port_ClearRecivedFlag()\
    {\
        Port0_ClearRecivedFlag();\
        Port1_ClearRecivedFlag();\
    }
    // 返回收到的一个字节数据
    STATIC_IN_LINE u8 Port_GetRecivedbyte()
    {
        if(mReciveDataPortNum == 0) return Port0_GetRecivedbyte();
        if(mReciveDataPortNum == 1) return Port1_GetRecivedbyte();
        return 0XFF;
    }

#elif (PORT_NUM == 3)
    #define Port_Init()\
    {\
        Port0_Init();\
        Port1_Init();\
        Port2_Init();\
    }
    #define Port_SendByte(a)\
    {\
        Port0_SendByte(a);\
        Port1_SendByte(a);\
        Port2_SendByte(a);\
    }
    #define Port_Send(a,b)\
    {\
        Port0_Send(a,b);\
        Port1_Send(a,b);\
        Port2_Send(a,b);\
    }
    #define Port_SetBaud(a)\
    {\
        Port0_SetBaud(a);\
        Port1_SetBaud(a);\
        Port2_SetBaud(a);\
    }
    // 收到数据返回1，否则返回0，用mReciveDataPortNum保存收到数据的端口号。
    STATIC_INLINE int Port_GetRecivedFlag()
    {
        if(mPortLocked) // 端口已经锁定，只监视锁定的端口
        {
            if(mReciveDataPortNum == 0) return Port0_GetRecivedFlag();
            if(mReciveDataPortNum == 1) return Port1_GetRecivedFlag();
            if(mReciveDataPortNum == 2) return Port2_GetRecivedFlag();
        }
        else
        {
            if(Port0_GetRecivedFlag()){mReciveDataPortNum = 0; return 1;}
            if(Port1_GetRecivedFlag()){mReciveDataPortNum = 1; return 1;}
            if(Port2_GetRecivedFlag()){mReciveDataPortNum = 2; return 1;}
        }
        return 0;
    }
    #define Port_ClearRecivedFlag()\
    {\
        Port0_ClearRecivedFlag();\
        Port1_ClearRecivedFlag();\
        Port2_ClearRecivedFlag();\
    }
    // 返回收到的一个字节数据
    STATIC_INLINE u8 Port_GetRecivedbyte()
    {
        if(mReciveDataPortNum == 0) return Port0_GetRecivedbyte();
        if(mReciveDataPortNum == 1) return Port1_GetRecivedbyte();
        if(mReciveDataPortNum == 2) return Port2_GetRecivedbyte();
        return 0XFF;
    }

#elif (PORT_NUM == 4)
    #define Port_Init()\
    {\
        Port0_Init();\
        Port1_Init();\
        Port2_Init();\
        Port3_Init();\
    }
    #define Port_SendByte(a)\
    {\
        Port0_SendByte(a);\
        Port1_SendByte(a);\
        Port2_SendByte(a);\
        Port3_SendByte(a);\
    }
    #define Port_Send(a,b)\
    {\
        Port0_Send(a,b);\
        Port1_Send(a,b);\
        Port2_Send(a,b);\
        Port3_Send(a,b);\
    }
    #define Port_SetBaud(a)\
    {\
        Port0_SetBaud(a);\
        Port1_SetBaud(a);\
        Port2_SetBaud(a);\
        Port3_SetBaud(a);\
    }
    // 收到数据返回1，否则返回0，用mReciveDataPortNum保存收到数据的端口号。
    STATIC_INLINE int Port_GetRecivedFlag()
    {
        if(mPortLocked) // 端口已经锁定，只监视锁定的端口
        {
            if(mReciveDataPortNum == 0) return Port0_GetRecivedFlag();
            if(mReciveDataPortNum == 1) return Port1_GetRecivedFlag();
            if(mReciveDataPortNum == 2) return Port2_GetRecivedFlag();
            if(mReciveDataPortNum == 3) return Port3_GetRecivedFlag();
        }
        else
        {
            if(Port0_GetRecivedFlag()){mReciveDataPortNum = 0; return 1;}
            if(Port1_GetRecivedFlag()){mReciveDataPortNum = 1; return 1;}
            if(Port2_GetRecivedFlag()){mReciveDataPortNum = 2; return 1;}
            if(Port3_GetRecivedFlag()){mReciveDataPortNum = 3; return 1;}
        }
        return 0;
    }
    #define Port_ClearRecivedFlag()\
    {\
        Port0_ClearRecivedFlag();\
        Port1_ClearRecivedFlag();\
        Port2_ClearRecivedFlag();\
        Port3_ClearRecivedFlag();\
    }
    // 返回收到的一个字节数据
    STATIC_INLINE u8 Port_GetRecivedbyte()
    {
        if(mReciveDataPortNum == 0) return Port0_GetRecivedbyte();
        if(mReciveDataPortNum == 1) return Port1_GetRecivedbyte();
        if(mReciveDataPortNum == 2) return Port2_GetRecivedbyte();
        if(mReciveDataPortNum == 3) return Port3_GetRecivedbyte();
        return 0XFF;
    }

#else
    #error "PORT_NUM 目前最大只能为4."
#endif


#elif (PORT_NUM == 1)

#define Port_Init                   _CONNECT(Port0,_Init)
#define Port_SendByte               _CONNECT(Port0,_SendByte)
#define Port_Send                   _CONNECT(Port0,_Send)
#define Port_SetBaud                _CONNECT(Port0,_SetBaud)
#define Port_GetRecivedFlag         _CONNECT(Port0,_GetRecivedFlag)
#define Port_ClearRecivedFlag       _CONNECT(Port0,_ClearRecivedFlag)
#define Port_GetRecivedbyte         _CONNECT(Port0,_GetRecivedbyte)

#else
    #error "PORT_NUM 必须是大于0的整数。"
#endif



// 对外接口函数定义

void BootLoader_Start()
{
    u8 updata_ret = UPDATA_OK;
 
    // BootLoader 场景初始化
	Port_Init();		// 串口初始化
	FLASH_INIT;		// Flash 初始化
    USER_INIT;        // 用户初始化
    while(1)
    {
        // 喂狗
 
#if (PORT_NUM > 1)
        mPortLocked = false;
#endif
#if (WORK_MODE == 0)
        // 尝试升级
#ifdef USE_LOG
        Print("Try to update.\n");
#endif
        CHECK_PORT;          // 检查端口
        Port_SendByte(0x7F); // 发送已经准备好接收数据
        updata_ret = Updata();
#endif
        if (updata_ret == UPDATA_NO || updata_ret == UPDATA_OK) // 没有对FLASH做任何操作 或者 升级成功
        {
            if (  (*(const u32 *)APP_FLAG_ADDR == APP_FLAG_DATA)
#if (USE_PER_TIMES_VERIFY == 1)
                &&((*(const u32 *)APP_LEN_ADDR) <= APP_LEN_MAX) // 检查FLASH中保存的APP长度值是否合理
    #if (VERIFY_FORMULA == 0)
                &&(SumU32_Verify((const u8 *)APP_BASE_ADDR, *(const u32 *)APP_LEN_ADDR))
    #else
                &&(Crc_Verify32((const u8 *)APP_BASE_ADDR, *(const u32 *)APP_LEN_ADDR))
    #endif
#endif
            ) // 存在APP
            {
#ifdef USE_LOG
                Print("APP already exists. Jump to APP.\n");
#endif
                USER_DEINIT;                      // 用户反初始化
                SystemDeInit();                     // 环境重置：关键寄存器值恢复到复位状态
                GotoApp(APP_BASE_ADDR);             // 跳转到APP
            }
#ifdef USE_LOG
            else    // 不存在APP
            {
                Print("There is no APP.\n");
            }
#endif  
        }
        else // 出现错误，已经操作过FLASH，APP可能已经被损坏（大概率损坏）
        {
#ifdef USE_LOG
            Print("Updata error, APP is damaged.\n");
#endif                              
            FLASH_WritePage(0, APP_FLAG_ADDR);  // 抹掉存在APP的标记
        }
#if (WORK_MODE == 1)
        // 尝试升级
#ifdef USE_LOG
        Print("Try to update.\n");
#endif
        CHECK_PORT;          // 检查端口
        Port_SendByte(0x7F); // 发送已经准备好接收数据
        updata_ret = Updata();
#endif
    // 如果APP有问题就不跳转，循环尝试升级。
    }
}

// 私有函数定义

//#ifdef USE_LOG
void Print(const char *s)
{
    while(*s != 0)
    {
        Port_SendByte(*s);
        s++;
    }
}
//#endif

// 执行升级操作
// 返回值： 
//      UPDATA_NO       没有对FLASH做任何操作
//      UPDATA_OK       升级成功，APP 已经被重新改写，且校验成功
//      UPDATA_TIMEOUT  接收数据超时，已操作 FLASH ，APP应该已经损坏
//      UPDATA_CHKERR   APP数据校验错误，已操作 FLASH ，APP应该已经损坏
static u8 Updata()
{
    bool written = false;               // 标记是否擦写了FALSH
    u32 now_addr = APP_BASE_ADDR;       // 现在写的地址
#if (USE_RECEIVE_VERIFY == 1)
    u32 app_data_len;                   // 整个APP数据长度
#endif

    while (1)
    {
        if (RecivePkg()) // 接收一包数据
        {
            switch (mBuff[0]) // CMD
            {
            case 0:     // 新的APP信息  C X X X L L L L A A A A
#if (USE_RECEIVE_VERIFY == 1)
                app_data_len = *(u32 *)(mBuff+4);       // 固件总长度
    #if(USE_LENGTH_CHECK == 1)
                if(app_data_len < APP_FLAG_ADDR + 4 - APP_BASE_ADDR)
                {
                    mBuff[1] = UPDATA_SIZEERR;
                    SendPkg(2, mBuff);
                    return UPDATA_NO;
                }
                else
    #endif
                {
                    mBuff[1] = 0;
                    SendPkg(2, mBuff);
                }
#else
                mBuff[1] = 0;
                SendPkg(2, mBuff);
#endif
                break;
            case 1: // APP 数据，除非是最后一包，否则一包刚好 FLASH_PAGE_SIZE 字节 C X X X D D D ...
#if (USE_AES == 1)
                if(now_addr >= AES_START_ADDR)
                    Aes_Decrypt(mBuff + 4, FLASH_PAGE_SIZE, AES_KEY_ARRY);
#endif
#if (USE_HJM_ENCRYPT == 1)
                if(now_addr >= ENCRYPT_START_ADDR)
                    Hjm_DecryptData(FLASH_PAGE_SIZE, mBuff + 4); // 解密数据
#endif
                FLASH_WritePage(mBuff + 4, now_addr);
                now_addr += FLASH_PAGE_SIZE;
                written = true; // 标记已经写过 FLASH
                mBuff[1] = 0;
                SendPkg(2, mBuff);
                break;

            case 2: // 传输结束 C X X X S S S S 
#if (USE_RECEIVE_VERIFY == 1)
    #if (VERIFY_FORMULA == 0)
                if(SumU32_Verify((const u8 *)APP_BASE_ADDR, app_data_len))    // 校验通过
    #else
                if(Crc_Verify32((const u8 *)APP_BASE_ADDR, app_data_len)) // 校验通过
    #endif
                {
                    mBuff[1] = 0;
                    SendPkg(2, mBuff);
                    return UPDATA_OK;
                }
                else    // 校验未通过
                {
                    mBuff[1] = UPDATA_CHKERR;
                    SendPkg(2, mBuff);
                    return UPDATA_CHKERR;
                }
#else
                mBuff[1] = 0;
                SendPkg(2, mBuff);
                return UPDATA_OK;
#endif
                break;
            default:
                break;
            }
        }
        else // 接收出错，超时
        {
            if (written == true)
                return UPDATA_TIMEOUT;
            else
                return UPDATA_NO;
        }
    }
}

static void SendPkg(u16 lenght,u8 *dat)	//发送协议数据包
{
    WAIT_BEFORE_RESPONSE;
	Port_SendByte(0xAA);
	Port_SendByte(0x55);
	Port_SendByte(lenght>>8);
	Port_SendByte(lenght);
	Port_Send(dat,lenght);
}

// 接收一个数据包
// 接收成功返回 true
static bool RecivePkg()
{
    int state = 0;
    int index = 0;
    u8 rx_dat;

    while (1)
    {
        if (ReciveByte(&rx_dat)) // 接收1个字节
        {
            switch (state)
            {
            case 0: //接收头H
                if (rx_dat == 0xAA)
                {
                    state = 1; //进入下一步
#if (PORT_NUM > 1)
                    mPortLocked = true;  // 先收到 AA 的端口会被锁定
#endif
                }
                break;

            case 1: //接收头L
                if (rx_dat == 0x55)
                {
                    state = 2; //进入下一步
                }
                else if (rx_dat == 0xAA)
                {
                    state = 1; //继续这一步
                }
                else
                {
                    state = 0; //重新开始接收头H
                    index = 0;
                }
                break;

            case 2: //接收长度
                index++;
                if (index == 1) // 长度H
                {
                    mPkgLen = (u16)rx_dat << 8;
                }
                else if (index == 2) // 长度L
                {
                    mPkgLen += rx_dat;
                    if (mPkgLen == 0)
                        state = 0; //重新开始接收头H
                    else
                        state = 3;
                    index = 0;
                }
                break;

            case 3: //接收数据
                mBuff[index++] = rx_dat;
                if (index == mPkgLen) //一包数据接收完毕，处理数据
                {
                    return true;
                }
                break;

            default:
                state = 0;
                index = 0;
                break;
            }
        }
        else // 一直没有数据，接收超时
        {
            return false;
        }
    }
    // return false;
}

static bool ReciveByte(u8 *byte)
{
    u32 i = 0;
    // 等待接收到数据
    while (Port_GetRecivedFlag() == 0)
    {
        i++;
        if (i == RECIVE_BYTE_TIMEOUT)
            return false;
    }
    Port_ClearRecivedFlag();
    *byte = Port_GetRecivedbyte();
    return true;
}




