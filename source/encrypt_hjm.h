#ifndef _ENCRYPT_HJM_H_
#define _ENCRYPT_HJM_H_

#include "typedef.h"
#include "config.h"

#define HJM_KEY0        (u8)((HJM_KEY)&0xFF)
#define HJM_KEY1        (u8)((HJM_KEY>>8)&0xFF)
#define HJM_KEY2        (u8)((HJM_KEY>>16)&0xFF)


void Hjm_EncryptData(u8 *buff, u16 len);
void Hjm_DecryptData(u16 len, u8 *dat);


#endif
