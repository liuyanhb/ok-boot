/***********************************************************************************
 * 文件名： main.c
 * 作者： 刘言
 * 版本： 1
 * 说明：
 * 		一个小而美的BootLoader，FLASH占用小于1KB，实现了OTA升级，APP数据解密与校验。
 * 		在M0+内核的HC32F003(005)上测试通过，理论上支持其它M0+内核的MCU，小改一下应该很容易支持
 * 	Cortex-M全系列。
 * 		串口升级通信协议参考文档。
 * 		默认的串口配置见 drv_uart 串口驱动，不同的MCU会有不同的串口驱动，固定波特率。
 * 		中断向量地址默认在0X00000000，如果你的FLASH起始地址不是0，修改 FLASH_BASE 宏即可，
 * 	比如 STM32 是 0X08000000。M0内核没有VTOR寄存器，参考具体厂家的文档。
 * 		更多的参数设置见 config.h 。
 * 	
 * 修改记录：
 * 	2020/7/2: 初版。 刘言。
 * 	2020/8/3: 简化了函数调用。刘言。
***********************************************************************************/

#include "mcu.h"
#include "BootLoader.h"


int main(void)
{
	// 看门狗初始化
	
	// 进入BootLoader
	BootLoader_Start();
}



