
#include "encrypt_hjm.h"


// 加密数据
// key:作为密码加密数据,3字节
// len: 数据长度  
// buff:数据。 
void Hjm_EncryptData(u8 *buff, u16 len)
{
	u16 i;
	byte passcode = HJM_KEY2;

	passcode = passcode ^ HJM_KEY0;
	passcode = passcode ^ HJM_KEY1;
	for(i = 0; i < len; i++)
	{
		buff[i] = buff[i] + passcode;
	}
}


// 解密数据，还原被 Hjm_EncryptData 加密过的数据
// key:密码,3字节
// len:数据长度  
// dat:数据。 
void Hjm_DecryptData(u16 len, u8 *dat)
{
	u16 i;
	byte passcode = HJM_KEY2;
 
	passcode = passcode ^ HJM_KEY0;
	passcode = passcode ^ HJM_KEY1;
	for(i = 0; i < len; i++)
	{
		dat[i] = dat[i] - passcode;
	}
}
